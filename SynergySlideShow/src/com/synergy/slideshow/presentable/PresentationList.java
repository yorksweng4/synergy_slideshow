/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow.presentable;

import java.util.ArrayList;

/**
 * This implementation of Presentable is the data structure equivalent of a pinboard xml.
 * 
 * @author Eddy
 * 
 */
public class PresentationList implements Presentable {

	private ArrayList<Presentation> presentations;
	private String id;
	private double width, height;

	/**
	 * PresentationList constructor.
	 * 
	 * @param id the id of the pinboard.
	 * @param width the width of the pinboard slides.
	 * @param height the height of the pinboard slides.
	 */
	public PresentationList(String id, double width, double height) {
		this.id = id;
		this.width = width;
		this.height = height;
		presentations = new ArrayList<Presentation>();
	}
	
	/**
	 * Construct an empty PresentationList.
	 */
	public PresentationList(){
		
	}

	/**
	 * Add a presentation to the pinboard.
	 * 
	 * @param p presentation to be added.
	 */
	public void addPresentation(Presentation p) {
		presentations.add(p);
	}

	@Override
	public ArrayList<Presentation> getPresentations() {
		return presentations;
	}

	@Override
	public double getWidth() {
		return width;
	}

	@Override
	public double getHeight() {
		return height;
	}

	/**
	 * @return the id of the presentation.
	 */
	public String getId() {
		return id;
	}
}
