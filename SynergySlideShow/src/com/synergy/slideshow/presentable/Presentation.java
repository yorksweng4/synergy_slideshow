/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow.presentable;

import java.util.ArrayList;

import com.synergy.slideshow.slide.Slide;

/**
 * This implementation of Presentable is the data structure directly equivalent to a presentation XML
 * file.
 * 
 * @author Eddy
 * 
 */
public class Presentation implements Presentable {

	private ArrayList<Slide> slides;
	private String title, author;
	private double width, height;

	/**
	 * Construct a fully populated presentation.
	 * 
	 * @param slides the presentation slides.
	 * @param title the title of the presentation.
	 * @param author the author of the presentation.
	 * @param width width of presentation.
	 * @param height height of presentation.
	 */
	public Presentation(ArrayList<Slide> slides, String title, String author, double width, double height) {
		this.slides = slides;
		this.title = title;
		this.author = author;
		this.width = width;
		this.height = height;
	}
	
	/**
	 * Construct an empty presentation.
	 */
	public Presentation(){
		
	}

	/**
	 * @return the slides.
	 */
	public ArrayList<Slide> getSlides() {
		return slides;
	}

	/**
	 * @return a slide.
	 */
	public Slide getSlide(int index) {
		return slides.get(index);
	}

	/**
	 * @return the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the author.
	 */
	public String getAuthor() {
		return author;
	}

	@Override
	public double getWidth() {
		return width;
	}

	@Override
	public double getHeight() {
		return height;
	}

	/**
	 * @param slides the slides to set.
	 */
	public void setSlides(ArrayList<Slide> slides) {
		this.slides = slides;
	}

	/**
	 * @param title the title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @param author the author to set.
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @param width the width to set.
	 */
	public void setWidth(double width) {
		this.width = width;
	}

	/**
	 * @param height the height to set.
	 */
	public void setHeight(double height) {
		this.height = height;
	}

	@Override
	public ArrayList<Presentation> getPresentations() {
		ArrayList<Presentation> pres = new ArrayList<Presentation>();
		pres.add(this);
		return pres; 
	}
}