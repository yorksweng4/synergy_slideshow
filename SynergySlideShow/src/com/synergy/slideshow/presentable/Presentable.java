/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow.presentable;

import java.util.ArrayList;

/**
 * 
 * This interface defines the behaviour of an object which can presented as a slideshow.
 * 
 * @author Eddy
 *
 */
public interface Presentable {

	/**
	 * @return an ArrayList containing all the presentations to be displayed (at least one Presentation is necessary).
	 */
	public ArrayList<Presentation> getPresentations();
	
	/**
	 * @return the width of the presentable object.
	 */
	public double getWidth();
	
	/**
	 * @return the height of the presentable object.
	 */
	public double getHeight();
	
}
