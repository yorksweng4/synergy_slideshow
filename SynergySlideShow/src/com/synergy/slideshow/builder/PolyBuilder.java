/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow.builder;

import java.util.ArrayList;
import java.util.HashMap;

import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.PolygonBuilder;

import org.xml.sax.Attributes;

import com.synergy.slideshow.slide.Instruction;


/**
 * This class builds a polygon content object and returns it
 * to the slide so it can be added. 
 * 
 * @author Ben
 *
 */
public class PolyBuilder {

	private Polygon poly;
	private Instruction instruction;

	/**
	 * @param attributes the parsed attributes, simply pass the whole object and it will be dealt with here.
	 * @param defaultValues the default values defined in the parser.
	 */
	public PolyBuilder(Attributes attributes, HashMap<String, String> defaultValues){

		int st = Integer.parseInt(defaultValues.get("polygonstarttime")), et = Integer.parseInt(defaultValues.get("polygonduration"));
		String id = defaultValues.get("polygonid"), strokeColour = defaultValues.get("polygonlinecolor"), fillColour = defaultValues.get("polygonfillcolor"), strokeWidth = defaultValues.get("polygonlinethickness");

		//Scan through attributes. If a relevant one exists and it's not empty, overwrite the default.
		for(int i = 0; i < attributes.getLength(); i++){
			if(attributes.getLocalName(i).equalsIgnoreCase("ID")){
				if(!attributes.getValue(i).isEmpty()){
					id = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("lineThickness")){
				if(!attributes.getValue(i).isEmpty()){
					strokeWidth = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("lineColor")){
				if(!attributes.getValue(i).isEmpty()){
					strokeColour = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("fillColor")){
				if(!attributes.getValue(i).isEmpty()){
					fillColour = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("startTime")){
				if(!attributes.getValue(i).isEmpty()){
					st = Integer.parseInt(attributes.getValue(i));
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("Duration")){
				if(!attributes.getValue(i).isEmpty()){
					et = Integer.parseInt(attributes.getValue(i));
				}
			}
		}

		poly = PolygonBuilder.create()
				.id(id)
				.build();

		// Exception catch for illegal colours
		try {
			poly.setStroke(Color.web(strokeColour));
		} catch (IllegalArgumentException e) {
			poly.setStroke(Color.web("000000"));
		}

		// Exception catch for illegal colours
		try {
			poly.setFill(Color.web(fillColour));
		} catch (IllegalArgumentException e) {
			poly.setFill(Color.web("000000"));
		}

		poly.setStrokeWidth(Double.parseDouble(strokeWidth));

		instruction = new Instruction(poly, st, et);

	}

	/**
	 * Add a point to the polygon.
	 * 
	 * @param pointX x-coordinate of point.
	 * @param pointY y-coordinate of point.
	 */
	public void addPoint(double pointX, double pointY){
		poly.getPoints().addAll(pointX, pointY);
	}
	
	/**
	 * Add a set of points to the polygon.
	 * 
	 * @param points an array of points to be added.
	 */
	public void addPoint(ArrayList<Double> points){
		poly.getPoints().addAll(points);
	}
	
	/**
	 * @return whether any points have been added to the polygon.
	 */
	public boolean areTherePoints(){
		
		return poly.getPoints().size() > 0;
	}

	/**
	 * @return instruction for built object.
	 */
	public Instruction getInstruction(){
		return instruction;
	}

	/**
	 * @return built the built Polygon object.
	 */
	public Polygon getBuilt(){
		return poly;
	}

}
