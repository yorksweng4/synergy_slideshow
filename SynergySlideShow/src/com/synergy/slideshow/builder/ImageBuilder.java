/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow.builder;

import java.util.HashMap;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import org.xml.sax.Attributes;

/**
 * 
 * This class builds image content objects and returns them.
 * 
 * @author Ben, Eddy
 *
 */
public class ImageBuilder {

	private ImageView iv;

	/**
	 * @param attributes the parsed attributes, simply pass the whole object and it will be dealt with here.
	 * @param defaultValues the defaults from the parser.
	 */
	public ImageBuilder(Attributes attributes, HashMap<String, String> defaultValues) {

		//Assign attribute variables
		//Default to be overwritten (hopefully)
		String imageID = defaultValues.get("imageid");
		String fileLocation = defaultValues.get("imagefilelocation");
		String xPos = defaultValues.get("imagexpos");
		String yPos = defaultValues.get("imageypos");
		String xSize = defaultValues.get("imagexsize");
		String ySize = defaultValues.get("imageysize");

		//Scan through attributes. If a relevant one exists and it's not empty, overwrite the default.
		for(int i = 0; i < attributes.getLength(); i++){
			if(attributes.getLocalName(i).equalsIgnoreCase("ID")){
				if(!attributes.getValue(i).isEmpty()){
					imageID = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("FileLocation")){
				if(!attributes.getValue(i).isEmpty()){
					fileLocation = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("xPos")){
				if(!attributes.getValue(i).isEmpty()){
					xPos = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("yPos")){
				if(!attributes.getValue(i).isEmpty()){
					yPos = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("xSize")){
				xSize = attributes.getValue(i);
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("ySize")){
				ySize = attributes.getValue(i);
			}
		}

		//Create new image view, pass the file.
		Image i;
		try {
			i = new Image(fileLocation);
		} catch(IllegalArgumentException e){
			i = new Image(defaultValues.get("imagefilelocation"));
		}
		
		iv = new ImageView(i);
		
		//Add some attributes.
		iv.setId(imageID);
		iv.setLayoutX(Double.parseDouble(xPos));
		iv.setLayoutY(Double.parseDouble(yPos));
		//If an image size tag is blank, keep the original size of the image.
		if(!xSize.isEmpty()){
			iv.setFitWidth(Double.parseDouble(xSize));
		}
		if(!ySize.isEmpty()){
			iv.setFitHeight(Double.parseDouble(ySize));
		}
		
		iv.setPreserveRatio(xSize.isEmpty() || ySize.isEmpty());

		//Set invisible
		iv.setVisible(true);
	}

	/**
	 * Use this constructor from the AlbumBuilder.
	 * 
	 * @param imageID
	 * @param fileLocation
	 * @param xSize
	 * @param ySize
	 */
	public ImageBuilder(String imageID, String fileLocation, double xSize, double ySize) {
		iv = new ImageView(new Image(fileLocation));
		iv.setId(imageID);
		iv.setFitHeight(ySize);
		iv.setFitWidth(xSize);
	}

	/**
	 * @return built objects, ready to be added to slide.
	 */
	public ImageView getBuilt(){
		return iv;
	}

}
