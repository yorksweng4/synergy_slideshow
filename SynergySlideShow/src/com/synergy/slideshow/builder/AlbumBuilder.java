/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow.builder;

import java.util.HashMap;

import org.xml.sax.Attributes;

import com.synergy.slideshow.slide.Album;
import com.synergy.slideshow.slide.Instruction;

/**
 * Used as an internal interface to allow the parser to construct individual albums.
 * 
 * @author Rory Tulip, Ben Cowperthwaite
 */
public class AlbumBuilder {

	private Album album;
	private Instruction instruction;

	/**
	 * @param attributes attributes parsed from XML file.
	 * @param defaultValues default values from parser.
	 */
	public AlbumBuilder(Attributes attributes, HashMap<String,String> defaultValues){

		// Attributes are assigned to appropriately named variables
		double xPos, yPos, xSize, ySize;
		int st, et;
		String id;

		//Defaults are set here, for any empty fields
		xPos = Double.parseDouble(defaultValues.get("albumxpos"));
		yPos = Double.parseDouble(defaultValues.get("albumypos"));
		xSize = Double.parseDouble(defaultValues.get("albumxsize"));
		ySize = Double.parseDouble(defaultValues.get("albumysize"));
		id = defaultValues.get("albumid");
		st = Integer.parseInt(defaultValues.get("albumstarttime"));
		et = Integer.parseInt(defaultValues.get("albumduration"));

		//Scan through attributes. If a relevant one exists and it's not empty, overwrite the default.
		for(int i = 0; i < attributes.getLength(); i++){
			if(attributes.getLocalName(i).equalsIgnoreCase("ID")){
				if(!attributes.getValue(i).isEmpty()){
					id = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("xPos")){
				if(!attributes.getValue(i).isEmpty()){
					xPos = Double.parseDouble(attributes.getValue(i));
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("yPos")){
				if(!attributes.getValue(i).isEmpty()){
					yPos = Double.parseDouble(attributes.getValue(i));
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("xSize")){
				if(!attributes.getValue(i).isEmpty()){
					xSize = Double.parseDouble(attributes.getValue(i));
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("ySize")){
				if(!attributes.getValue(i).isEmpty()){
					ySize = Double.parseDouble(attributes.getValue(i));
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("StartTime")){
				if(!attributes.getValue(i).isEmpty()){
					st = Integer.parseInt(attributes.getValue(i));
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("Duration")){
				if(!attributes.getValue(i).isEmpty()){
					et = Integer.parseInt(attributes.getValue(i));
				}
			}
		}

		// A new album is initialised
		album = new Album();
		album.setStyle("-fx-background-color: black;");
		album.setPrefSize(xSize, ySize);
		album.setLayoutX(xPos);
		album.setLayoutY(yPos);
		album.setId(id);
		album.buildInterface();

		// Make the instruction
		instruction = new Instruction(album, st, et);
	}

	/**
	 * @return Album the built album.
	 */
	public Album getBuilt(){
		return album;
	}

	/**
	 * @return Instruction the instruction for the built Album.
	 */
	public Instruction getInstruction(){
		return instruction;
	}

	/**
	 * This method adds a given image to the album's imageContainer. The
	 * required attributes for this function are translated into individual
	 * Strings so they can be referred to elsewhere in the project.
	 */
	public void addImage(Attributes attributes){

		//Defaults, to be overwritten (hopefully)
		String imageID = "DefaultImageID";
		String fileLocation = "http://benshouse.crabdance.com/xml/photos/imagenotfound.png";

		//Scan through attributes. If a relevant one exists and it's not empty, overwrite the default.
		for(int i = 0; i < attributes.getLength(); i++){
			if(attributes.getLocalName(i).equalsIgnoreCase("ID")){
				if(!attributes.getValue(i).isEmpty()){
					imageID = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("FileLocation")){
				if(!attributes.getValue(i).isEmpty()){
					fileLocation = attributes.getValue(i);
				}
			}
		}

		ImageBuilder imageBuild = new ImageBuilder(imageID, fileLocation, album.getPrefWidth(), album.getPrefHeight());
		album.addImage(imageBuild.getBuilt());
	}

	/**
	 * A command to the album to ready itself for viewing.
	 */
	public void finaliseAlbum(){
		album.showFirstImage();
	}
}
