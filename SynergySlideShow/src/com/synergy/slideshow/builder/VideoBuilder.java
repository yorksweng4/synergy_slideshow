/*
* Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
*/
package com.synergy.slideshow.builder;

import java.util.HashMap;

import org.xml.sax.Attributes;

import com.synergy.slideshow.slide.MediaObject;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;

/**
 * 
 * @author Ben, Matt (MediaControls)
 *
 */
public class VideoBuilder {
	
	private MediaView mv;
	private MediaObject mo;
	
	/**
	 * @param attributes - the parsed attributes, simply pass the whole object and it will be dealt with here.
	 * @param defaultValues the default values from the parser.
	 */
	public VideoBuilder(Attributes attributes, HashMap<String, String> defaultValues) {

		//Set up variables with default values.
		String ID = defaultValues.get("videoid"), 
				fileLocation = defaultValues.get("videofilelocation"),
				xPos = defaultValues.get("videoxpos"), 
				yPos =defaultValues.get("videoypos"),
				xSize = defaultValues.get("videoxsize"),
				ySize = defaultValues.get("videoysize");
		
		boolean controls = Boolean.parseBoolean(defaultValues.get("videoinlinecontrol"));
		
		//Where possible, overwrite default values
		//Scan through attributes. If a relevant one exists and it's not empty, overwrite the default.
		for(int i = 0; i < attributes.getLength(); i++){
			if(attributes.getLocalName(i).equalsIgnoreCase("ID")){
				if(!attributes.getValue(i).isEmpty()){
					ID = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("FileLocation")){
				if(!attributes.getValue(i).isEmpty()){
					fileLocation = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("xPos")){
				if(!attributes.getValue(i).isEmpty()){
					xPos = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("yPos")){
				if(!attributes.getValue(i).isEmpty()){
					yPos = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("xSize")){
				xSize = attributes.getValue(i);
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("ySize")){
				ySize = attributes.getValue(i);
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("InlineControl")){
				if(!attributes.getValue(i).isEmpty()){
					controls = Boolean.parseBoolean(attributes.getValue(i));
				}
			}
		}
		
		//Create video place holder and assign video from file, then add to pane.
		Media m;
		try{
			m = new Media(fileLocation);
		} catch(IllegalArgumentException e){
			m = new Media(defaultValues.get("videofilelocation"));
		}
		MediaPlayer mp = new MediaPlayer(m);
		//Play automatically.
		mp.setAutoPlay(false);
		
		//Create media and add media player to it.
		mv = new MediaView(mp);
		
		//set mv size
		if(!xSize.isEmpty()){
			mv.setFitWidth(Double.parseDouble(xSize));
		}
		if(!ySize.isEmpty()){
			mv.setFitHeight(Double.parseDouble(ySize));
		}
		
		mv.setPreserveRatio(xSize.isEmpty() || ySize.isEmpty());
		
		// instantiate MediaObject
		mo = new MediaObject(mv);
		
		//Assign a smattering of attributes.
		mo.setId(ID);
		mo.setLayoutX(Double.parseDouble(xPos));
		mo.setLayoutY(Double.parseDouble(yPos));
		
		// Add controls if required
		if(controls){
			mo.addControls();
		}
		
	}
	
	/**
	 * @return built objects, ready to be added to slide.
	 */
	public MediaObject getBuilt(){
		return mo;
	}
}
