/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow.ui;

import com.sun.javafx.scene.traversal.Direction;
import com.synergy.slideshow.SlideShow;
import com.synergy.slideshow.slide.Slide;

import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.input.MouseEvent;

/**
 * This class deals with mouse input from the user. It requires a reference to
 * the slideshow to be handled.
 * 
 * @author Eddy, Kat
 * 
 */
public class DragEventHandler implements EventHandler<MouseEvent> {

	private Orientation dir;
	private double mouseX, mouseY, sceneX, sceneY;
	private SlideShow parent;
	private Slide currentSlide;

	/**
	 * @param s the slideshow to be controlled.
	 *           
	 */
	public DragEventHandler(SlideShow s) {
		parent = s;
	}

	@Override
	public void handle(MouseEvent e) {
		// only drag if slideshow is not busy
		if(!parent.isBusy() && !parent.getCurrentSlide().isMoving()){
			if (e.getEventType() == MouseEvent.MOUSE_PRESSED) {

				// store click position in relation to slide
				mouseX = e.getX();
				mouseY = e.getY();

				// store position in relation to scene
				sceneX = e.getSceneX();
				sceneY = e.getSceneY();

				// get current slide for later comparisons
				currentSlide = parent.getCurrentSlide();

			} else if ((e.getEventType() == MouseEvent.MOUSE_DRAGGED) && (currentSlide == parent.getCurrentSlide())) {

				// dir is initially null, so we set it here based on the user's
				// first motion
				if (dir == null) {
					dir = (Math.abs(sceneX - e.getSceneX()) > Math.abs(sceneY
							- e.getSceneY())) ? Orientation.HORIZONTAL
									: Orientation.VERTICAL;

					// we also set up the slides for dragging
					prepareDrag();
				}
				// here it gets a little convoluted, but bear with it: when the drag
				// direction equals the slideshow configuration, we are dragging
				// slides; else, we are dragging presentations
				else if (dir == parent.getOrientation()) {
					currentSlide
					.setTranslateX((parent.getOrientation() == Orientation.HORIZONTAL) ? e
							.getSceneX() - mouseX
							: 0);
					currentSlide
					.setTranslateY((parent.getOrientation() == Orientation.VERTICAL) ? e
							.getSceneY() - mouseY
							: 0);

					// translate next and previous slides, if they exist
					if (parent.isThereNextSlide()) {
						parent.getNextSlide().setTranslateX((parent.getOrientation() == Orientation.HORIZONTAL) ? e.getSceneX()	- mouseX + parent.getPrefWidth() : 0);
						parent.getNextSlide().setTranslateY((parent.getOrientation() == Orientation.VERTICAL) ? e.getSceneY() - mouseY + parent.getPrefHeight() : 0);
					}

					if (parent.isTherePreviousSlide()) {
						parent.getPreviousSlide()
						.setTranslateX(
								(parent.getOrientation() == Orientation.HORIZONTAL) ? e
										.getSceneX()
										- mouseX
										- parent.getPrefWidth() : 0);
						parent.getPreviousSlide()
						.setTranslateY(
								(parent.getOrientation() == Orientation.VERTICAL) ? e
										.getSceneY()
										- mouseY
										- parent.getPrefHeight() : 0);

					}

				} else {
					currentSlide
					.setTranslateX((parent.getOrientation() == Orientation.VERTICAL) ? e
							.getSceneX() - mouseX
							: 0);
					currentSlide
					.setTranslateY((parent.getOrientation() == Orientation.HORIZONTAL) ? e
							.getSceneY() - mouseY
							: 0);

					// translate next and previous slides, if they exist
					if (parent.isThereNextPresentation()) {
						parent.getNextPresentation()
						.getSlide(0)
						.setTranslateX(
								(parent.getOrientation() == Orientation.VERTICAL) ? e
										.getSceneX()
										- mouseX
										+ parent.getPrefWidth() : 0);
						parent.getNextPresentation()
						.getSlide(0)
						.setTranslateY(
								(parent.getOrientation() == Orientation.HORIZONTAL) ? e
										.getSceneY()
										- mouseY
										+ parent.getPrefHeight() : 0);


					}

					if (parent.isTherePreviousPresentation()) {
						parent.getPreviousPresentation()
						.getSlide(0)
						.setTranslateX(
								(parent.getOrientation() == Orientation.VERTICAL) ? e
										.getSceneX()
										- mouseX
										- parent.getPrefWidth() : 0);
						parent.getPreviousPresentation()
						.getSlide(0)
						.setTranslateY(
								(parent.getOrientation() == Orientation.HORIZONTAL) ? e
										.getSceneY()
										- mouseY
										- parent.getPrefHeight() : 0);

					}
				}

			} else if (e.getEventType() == MouseEvent.MOUSE_RELEASED){

				if (currentSlide == parent.getCurrentSlide()) {

					// drag direction equals configuration
					if (dir == parent.getOrientation()) {
							if (dir == Orientation.HORIZONTAL) {
								// we have moved far enough to go to next slide
								if ((e.getSceneX() - mouseX < -parent.getPrefWidth() / 3)) {
									if(parent.isTherePreviousSlide()){
										parent.getPreviousSlide().exitTo(Direction.LEFT);
									}
									parent.goToNextSlide();

								} 
								// we have moved far enough to go to previous slide
								else if(e.getSceneX() - mouseX > parent.getPrefWidth() / 3) {
									if (parent.isThereNextSlide()) {
										parent.getNextSlide().exitTo(Direction.RIGHT);
									}
									parent.goToPreviousSlide();
								}
								// we have not moved far enough to change slides
								else {
									if (parent.isThereNextSlide()) {
										parent.getNextSlide().exitTo(Direction.RIGHT);
									}
									if(parent.isTherePreviousSlide()){
										parent.getPreviousSlide().exitTo(Direction.LEFT);
									}
								}
							
							} else if(dir == Orientation.VERTICAL) {
								// we have moved far enough to go to next slide
								if ((e.getSceneY() - mouseY < -parent.getPrefHeight() / 3)) {
									if(parent.isTherePreviousSlide()){
										parent.getPreviousSlide().exitTo(Direction.UP);
									}
									parent.goToNextSlide();

								} 
								// we have moved far enough to go to previous slide
								else if(e.getSceneY() - mouseY > parent.getPrefHeight() / 3) {
									if (parent.isThereNextSlide()) {
										parent.getNextSlide().exitTo(Direction.DOWN);
									}
									parent.goToPreviousSlide();
								}
								// we have not moved far enough to change slides
								else {
									if (parent.isThereNextSlide()) {
										parent.getNextSlide().exitTo(Direction.DOWN);
									}
									if(parent.isTherePreviousSlide()){
										parent.getPreviousSlide().exitTo(Direction.UP);
									}
								}
							}

					} else {
						if (dir == Orientation.HORIZONTAL) {
							// we have moved far enough to go to next presentation
							if ((e.getSceneX() - mouseX < -parent.getPrefWidth() / 3)) {
								if(parent.isTherePreviousPresentation()){
									parent.getPreviousPresentation().getSlide(0).exitTo(Direction.LEFT);
								}
								parent.goToNextPresentation();

							} 
							// we have moved far enough to go to previous presentation
							else if(e.getSceneX() - mouseX > parent.getPrefWidth() / 3) {
								if (parent.isThereNextPresentation()) {
									parent.getNextPresentation().getSlide(0).exitTo(Direction.RIGHT);
								}
								parent.goToPreviousPresentation();
							}
							// we have not moved far enough to change presentation
							else {
								if (parent.isThereNextPresentation()) {
									parent.getNextPresentation().getSlide(0).exitTo(Direction.RIGHT);
								}
								if(parent.isTherePreviousPresentation()){
									parent.getPreviousPresentation().getSlide(0).exitTo(Direction.LEFT);
								}
							}
						
						} else if(dir == Orientation.VERTICAL) {
							// we have moved far enough to go to next presentation
							if ((e.getSceneY() - mouseY < -parent.getPrefHeight() / 3)) {
								if(parent.isTherePreviousPresentation()){
									parent.getPreviousPresentation().getSlide(0).exitTo(Direction.UP);
								}
								parent.goToNextPresentation();

							} 
							// we have moved far enough to go to previous presentation
							else if(e.getSceneY() - mouseY > parent.getPrefHeight() / 3) {
								if (parent.isThereNextPresentation()) {
									parent.getNextPresentation().getSlide(0).exitTo(Direction.DOWN);
								}
								parent.goToPreviousPresentation();
							}
							// we have not moved far enough to change presentation
							else {
								if (parent.isThereNextPresentation()) {
									parent.getNextPresentation().getSlide(0).exitTo(Direction.DOWN);
								}
								if(parent.isTherePreviousPresentation()){
									parent.getPreviousPresentation().getSlide(0).exitTo(Direction.UP);
								}
								
							}
						}
					}
					
					// if current slide hasnt changed, move it back to the centre - where from is irrelevant 
					if(currentSlide == parent.getCurrentSlide() && dir != null){
						currentSlide.enterFrom(null);
					}

				}

				// direction is reset
				dir = null;

			}
		}
	}

	/**
	 * This method prepares the necessary slides for dragging. 
	 */
	private void prepareDrag() {

		// current slide is being dragged
		currentSlide.setDragging(true);
		
		// if drag direction equals slideshow configuration, we are dealing with
		// slides
		if (dir == parent.getOrientation()) {

			// next and previous slides are visible, if they exist, and they're
			// also where they need to be, and set as dragging
			if (parent.isThereNextSlide()) {
				parent.getNextSlide().setTranslateY((dir == Orientation.HORIZONTAL) ? 0 : parent.getPrefHeight());
				parent.getNextSlide().setTranslateX((dir == Orientation.VERTICAL) ? 0 : parent.getPrefWidth());
				parent.getNextSlide().setVisible(true);
				parent.getNextSlide().setDragging(true);
				parent.getNextSlide().resetContent();

			}

			if (parent.isTherePreviousSlide()) {
				parent.getPreviousSlide().setTranslateY(
						(dir == Orientation.HORIZONTAL) ? 0 : -parent
								.getPrefHeight());
				parent.getPreviousSlide().setTranslateX(
						(dir == Orientation.VERTICAL) ? 0 : -parent
								.getPrefWidth());
				parent.getPreviousSlide().setVisible(true);
				parent.getPreviousSlide().setDragging(true);
				parent.getPreviousSlide().resetContent();
			}

		}
		// if not, presentations it is
		else {
			// next and previous presentations are visible, if they exist, and
			// they're also where they need to be
			if (parent.isThereNextPresentation()) {

				parent.getNextPresentation()
				.getSlide(0)
				.setTranslateY(
						(dir == Orientation.HORIZONTAL) ? 0 : parent
								.getPrefHeight());
				parent.getNextPresentation()
				.getSlide(0)
				.setTranslateX(
						(dir == Orientation.VERTICAL) ? 0 : parent
								.getPrefWidth());
				parent.getNextPresentation().getSlide(0).setVisible(true);
				parent.getNextPresentation().getSlide(0).setDragging(true);
				parent.getNextPresentation().getSlide(0).resetContent();

			}

			if (parent.isTherePreviousPresentation()) {

				parent.getPreviousPresentation()
				.getSlide(0)
				.setTranslateY(
						(dir == Orientation.HORIZONTAL) ? 0 : -parent
								.getPrefHeight());
				parent.getPreviousPresentation()
				.getSlide(0)
				.setTranslateX(
						(dir == Orientation.VERTICAL) ? 0 : -parent.getPrefWidth());
				parent.getPreviousPresentation().getSlide(0).setVisible(true);
				parent.getPreviousPresentation().getSlide(0).setDragging(true);
				parent.getPreviousPresentation().getSlide(0).resetContent();

			}
		}

	}
}
