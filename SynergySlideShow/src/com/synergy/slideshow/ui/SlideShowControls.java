/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow.ui;

import com.synergy.slideshow.SlideShow;

import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * This is the extracted class containing the slideshow controls. This can be
 * changed freely as it is added when the slideshow builds the interface.
 * 
 * @author Eddy, Rory
 * 
 */
public class SlideShowControls extends Group {

	private SlideShow parent;
	private ImageView rightArrow, leftArrow, upArrow, downArrow;
	private boolean toGoUp = false, toGoDown = false, toGoLeft = false, toGoRight = false;

	/**
	 * Constructor takes a reference to the slideshow.
	 * 
	 * @param p a reference to the slideshow to be controlled.
	 */
	public SlideShowControls(SlideShow p) {
		// set parent reference
		this.parent = p;

		// prepare all buttons
		// left arrow
		leftArrow = new ImageView(new Image("left.png"));
		leftArrow.setOpacity(0.3);

		// size correctly - using a 19x19 grid
		leftArrow.setFitHeight(parent.getPrefHeight() / 19);
		leftArrow.setFitWidth(parent.getPrefWidth() / 19);

		// place correctly - using a 19x19 grid
		leftArrow.setLayoutX(0);
		leftArrow.setLayoutY(((parent.getPrefHeight() / 19) * 9));

		// right arrow
		rightArrow = new ImageView(new Image("right.png"));
		rightArrow.setOpacity(0.3);

		// size correctly - using a 19x19 grid
		rightArrow.setFitHeight(parent.getPrefHeight() / 19);
		rightArrow.setFitWidth(parent.getPrefWidth() / 19);

		// place correctly - using a 19x19 grid
		rightArrow.setLayoutX((parent.getPrefWidth() / 19) * 18);
		rightArrow.setLayoutY((parent.getPrefHeight() / 19) * 9);

		// down arrow
		downArrow = new ImageView(new Image("down.png"));
		downArrow.setOpacity(0.3);

		// size correctly - using a 19x19 grid
		downArrow.setFitHeight(parent.getPrefHeight() / 19);
		downArrow.setFitWidth(parent.getPrefWidth() / 19);

		// place correctly - using a 19x19 grid
		downArrow.setLayoutX((parent.getPrefWidth() / 19) * 9);
		downArrow.setLayoutY((parent.getPrefHeight() / 19) * 18);

		// up arrow
		upArrow = new ImageView(new Image("up.png"));
		upArrow.setOpacity(0.3);

		// size correctly - using a 19x19 grid
		upArrow.setFitHeight(parent.getPrefHeight() / 19);
		upArrow.setFitWidth(parent.getPrefWidth() / 19);

		// place correctly - using a 19x19 grid
		upArrow.setLayoutX((parent.getPrefWidth() / 19) * 9);
		upArrow.setLayoutY(0);

		// if user moves mouse, update opacity of arrows
		parent.addEventHandler(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
			
			@Override
			public void handle(MouseEvent e){

				// set the opacity to 1 if the cursor is in range, else set it to 0.3
				// if you don't like unreasonably long ternary operators, don't look down!
				upArrow.setOpacity((Math.abs((int)(e.getX() - parent.getPrefWidth()/2)) < parent.getPrefWidth() / 7 && e.getY() < 2*(parent.getPrefHeight()/7)) && ((parent.getOrientation() == Orientation.HORIZONTAL && parent.isTherePreviousPresentation()) || (parent.getOrientation() == Orientation.VERTICAL && parent.isTherePreviousSlide())) ? 1.0 : 0.3);
				downArrow.setOpacity((Math.abs((int)(e.getX() - parent.getPrefWidth()/2)) < parent.getPrefWidth() / 7 && e.getY() > 5*(parent.getPrefHeight()/7)) && ((parent.getOrientation() == Orientation.HORIZONTAL && parent.isThereNextPresentation()) || (parent.getOrientation() == Orientation.VERTICAL && parent.isThereNextSlide())) ? 1.0 : 0.3);
				leftArrow.setOpacity((Math.abs((int)(e.getY() - parent.getPrefHeight()/2)) < parent.getPrefHeight() / 7 && e.getX() < 2*(parent.getPrefWidth()/7)) && ((parent.getOrientation() == Orientation.HORIZONTAL && parent.isTherePreviousSlide()) || (parent.getOrientation() == Orientation.VERTICAL && parent.isTherePreviousPresentation())) ? 1.0 : 0.3);
				rightArrow.setOpacity((Math.abs((int)(e.getY() - parent.getPrefHeight()/2)) < parent.getPrefHeight() / 7 && e.getX() > 5*(parent.getPrefWidth()/7)) && ((parent.getOrientation() == Orientation.HORIZONTAL && parent.isThereNextSlide()) || (parent.getOrientation() == Orientation.VERTICAL && parent.isThereNextPresentation())) ? 1.0 : 0.3);
				
			}
		});

		// if mouse exits slideshow, reset opacity
		parent.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent e){
				upArrow.setOpacity(0.3);
				downArrow.setOpacity(0.3);
				leftArrow.setOpacity(0.3);
				rightArrow.setOpacity(0.3);
			}
		});

		// if mouse pressed, prepare to move
		parent.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent e){

				if (Math.abs((int)(e.getX() - parent.getPrefWidth()/2)) < parent.getPrefWidth() / 7) {
					if (e.getY() < 2*(parent.getPrefHeight()/7)) {
						toGoUp = true;
						toGoDown = false;
						toGoLeft = false;
						toGoRight = false;
					} else if (e.getY() > 5*(parent.getPrefHeight()/7)) {
						toGoUp = false;
						toGoDown = true;
						toGoLeft = false;
						toGoRight = false;
					} else {
						toGoUp = false;
						toGoDown = false;
						toGoLeft = false;
						toGoRight = false;
					}
				} else if (Math.abs((int)(e.getY() - parent.getPrefHeight()/2)) < parent.getPrefHeight() / 7) {
					if (e.getX() < 2*(parent.getPrefWidth()/7)) {
						toGoUp = false;
						toGoDown = false;
						toGoLeft = true;
						toGoRight = false;
					} else if (e.getX() > 5*(parent.getPrefWidth()/7)) {
						toGoUp = false;
						toGoDown = false;
						toGoLeft = false;
						toGoRight = true;
					} else {
						toGoUp = false;
						toGoDown = false;
						toGoLeft = false;
						toGoRight = false;
					}
				}
			}
		});

		// mouse released, change slides - or not
		parent.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent e){

				if (Math.abs((int)(e.getX() - parent.getPrefWidth()/2)) < parent.getPrefWidth() / 7) {
					if (e.getY() < 2*(parent.getPrefHeight()/7) && toGoUp == true) {
						parent.goUp();
					} else if (e.getY() > 5*(parent.getPrefHeight()/7) && toGoDown == true) {
						parent.goDown();
					}
				} else if (Math.abs((int)(e.getY() - parent.getPrefHeight()/2)) < parent.getPrefHeight() / 7) {
					if (e.getX() < 2*(parent.getPrefWidth()/7) && toGoLeft == true) {
						parent.goLeft();
					} else if (e.getX() > 5*(parent.getPrefWidth()/7) && toGoRight == true) {
						parent.goRight();
					}
				}

			}
		});

		// add all buttons
		getChildren().addAll(leftArrow, rightArrow, downArrow, upArrow);

		// refresh buttons
		refreshButtons();
	}

	/**
	 * Enable/disable buttons based on current slide and presentation.
	 */
	public void refreshButtons() {
		if (parent.getOrientation() == Orientation.HORIZONTAL) {
			leftArrow.setVisible(parent.isTherePreviousSlide());
			rightArrow.setVisible(parent.isThereNextSlide());
			downArrow.setVisible(parent.isThereNextPresentation());
			upArrow.setVisible(parent.isTherePreviousPresentation());

		} else if (parent.getOrientation() == Orientation.VERTICAL) {

			leftArrow.setVisible(parent.isTherePreviousPresentation());
			rightArrow.setVisible(parent.isThereNextPresentation());
			downArrow.setVisible(parent.isThereNextSlide());
			upArrow.setVisible(parent.isTherePreviousSlide());
		}
	}
}
