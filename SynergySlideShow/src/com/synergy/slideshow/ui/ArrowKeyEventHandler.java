/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow.ui;

import com.synergy.slideshow.SlideShow;

import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;

/**
 * This implementation of EventHandler<KeyEvent> deals with the arrow key events
 * in the context of the slideshow class.
 * 
 * @author Eddy, Kat 
 * 
 */
public class ArrowKeyEventHandler implements EventHandler<KeyEvent> {

	private SlideShow parent;

	/**
	 * Constructor takes a reference to the slideshow
	 * 
	 * @param s the slideshow to be controlled.
	 */
	public ArrowKeyEventHandler(SlideShow s) {
		parent = s;
	}

	@Override
	public void handle(KeyEvent ke) {
		// polymorphic design means keys do different things depending on how many presentations there are - but this is handled by the slideshow
		// if the slideshow is busy, do nothing
		if(!parent.isBusy() && !parent.getCurrentSlide().isDragging()){
			switch (ke.getCode()) {
			case RIGHT:
				parent.goRight();
				break;

			case LEFT:
				parent.goLeft();
				break;

			case DOWN:
				parent.goDown();
				break;

			case UP:
				parent.goUp();
				break;
				
			// a few generic control keys
			case ENTER:
				parent.goToNextSlide();
				
			case ESCAPE:
				parent.pause();

			default:
				break;
			}
		}
		// prevent traversing from happening
		ke.consume();
	}
}
