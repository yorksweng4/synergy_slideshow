/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow;

import javafx.event.Event;
import javafx.event.EventType;

/**
 * SlideShowEvent is instantiated and fired whenever a slideshow event occurs, such as pausing and starting.
 * This allows the user to specify custom actions upon pausing or starting a slideshow without having to modify the
 * slideshow itself.
 * 
 * @author Eddy
 *
 */
public class SlideShowEvent extends Event {

	private static final long serialVersionUID = -7254426542389335236L;
	
	public static final EventType<SlideShowEvent> SLIDESHOW_PAUSED = new EventType<SlideShowEvent>(ANY, "SLIDESHOW_PAUSED");
	public static final EventType<SlideShowEvent> SLIDESHOW_STARTED = new EventType<SlideShowEvent>(ANY, "SLIDESHOW_STARTED");

	/**
	 * @param e the event type, use SlideShowEvent.SLIDESHOW_PAUSED and SlideShowEvent.SLIDESHOW_STARTED.
	 */
	public SlideShowEvent(EventType<? extends Event> e) {
		super(e);
	}
	
}
