/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow;

import java.util.ArrayList;

import com.sun.javafx.scene.traversal.Direction;
import com.synergy.slideshow.presentable.Presentable;
import com.synergy.slideshow.presentable.Presentation;
import com.synergy.slideshow.slide.Slide;
import com.synergy.slideshow.ui.ArrowKeyEventHandler;
import com.synergy.slideshow.ui.DragEventHandler;
import com.synergy.slideshow.ui.SlideShowControls;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.util.Duration;

/**
 * This is the main class of the slideshow package. An instance of this
 * class will display a Presentable object, can be controlled by
 * default using arrow keys and clicking and dragging with the mouse, as well as
 * with the on-screen buttons.
 * 
 * This class extends Region, so it can be used with JavaFX like any other Node.
 * Note that a SlideShow instance cannot be traversed through as the
 * arrow key handlers consume the event.
 * 
 * Two basic implementations of Presentable are provided, Presentation and PresentationList.
 * While these allow for the full potential of the SlideShow to be explored, additional Presentables
 * can be defined and presented.
 * 
 * @authors Matt, Eddy, Kat, Mike
 * 
 */
public class SlideShow extends Region {

	private Pane slideContainer;

	private SlideShowControls slideShowControls;
	private ContentHandler handler;

	private ArrayList<Presentation> presentationList;
	private int currentSlide = 0;
	private int currentPresentation = 0;

	// configuration is horizontal by default
	private Orientation slideDirection = Orientation.HORIZONTAL;

	// busy, false by default
	private boolean busy = false;

	// event handlers for pause and start events
	private EventHandler<SlideShowEvent> onPause, onStart;

	/**
	 * This constructor instantiates a clean SlideShow with a new presentable. Any previously added Presentables
	 * are naturally replaced.
	 * 
	 * @param presentation the Presentable to be displayed.
	 */
	public SlideShow(Presentable presentation) {

		super();
		// only build interface if presentations exist
		if(presentation.getPresentations().size() > 0){
			
			// set size
			setPrefSize(presentation.getWidth(), presentation.getHeight());

			// initialize presentation list
			presentationList = presentation.getPresentations();

			// create interface - controls and slideContainer
			buildInterface();

			// add slides to slideContainer
			for (Presentation p : presentationList) {
				slideContainer.getChildren().addAll(p.getSlides());
			}
			// the thread
			handler = new ContentHandler(this);

			// slideshow starts off invisible, but first slide must be visible
			setVisible(false);
			getCurrentSlide().setVisible(true);
		}
	}

	/**
	 * Use this method to set custom behaviour for the slideshow upon pausing. This happens after the slideshow has fully paused.
	 * 
	 * @param eh the event handler to handle slideshow pausing.
	 */
	public void setOnPause(EventHandler<SlideShowEvent> eh) {
		removeEventHandler(SlideShowEvent.SLIDESHOW_PAUSED, onPause);
		onPause = eh;
		addEventHandler(SlideShowEvent.SLIDESHOW_PAUSED, onPause);
	}

	/**
	 * Use this method to set custom behaviour for the slideshow upon starting. This happens after the slideshow has fully started.
	 * 
	 * @param eh the event handler to handle slideshow starting.
	 */
	public void setOnStart(EventHandler<SlideShowEvent> eh) {
		removeEventHandler(SlideShowEvent.SLIDESHOW_STARTED, onStart);
		onStart = eh;
		addEventHandler(SlideShowEvent.SLIDESHOW_STARTED, onStart);

	}

	/**
	 * Set the slideshow direction style, use Orientation.HORIZONTAL
	 * or Orientation.VERTICAL. This controls the direction in which slides scroll.
	 * Scrolling between presentations will then happen along the other axis.
	 * 
	 * @param nc the new configuration.
	 */
	public void setOrientation(Orientation nc) {
		slideDirection = nc;

		// reset buttons just in case
		slideShowControls.refreshButtons();

	}

	/**
	 * @return the current configuration style, i.e. which way the slides should
	 *         scroll.
	 */
	public Orientation getOrientation() {
		return slideDirection;
	}

	/**
	 * Busy means slides are currently scrolling.
	 * 
	 * @return true if slideshow is busy, false if otherwise.
	 */
	public boolean isBusy() {
		return busy;
	}

	/**
	 * This method builds the slideshow interface, event handlers and all.
	 */
	private void buildInterface() {

		// set slideshow background
		setStyle("-fx-background-image: url('linen.png'), url('synergy_logo_white_dragon.png'); -fx-background-repeat: repeat, no-repeat; -fx-background-position: center, center; -fx-background-size: auto, 600");

		// build on-screen controls
		slideShowControls = new SlideShowControls(this);

		// exit button
		final Button exitSlideShow;

		exitSlideShow = new Button("X");
		exitSlideShow.setStyle("-fx-base: red; -fx-font-weight: bold;");
		exitSlideShow.setLayoutX(getPrefWidth() - 40);
		exitSlideShow.setLayoutY(5);

		// action listener, pause() on press
		exitSlideShow.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				pause();
			}

		});

		// initialize slideContainer
		slideContainer = new Pane();

		// arrow key controls for navigating the slideshow
		addEventFilter(KeyEvent.KEY_PRESSED, new ArrowKeyEventHandler(this));

		// mouse handlers for navigating the slideshow
		slideContainer.addEventHandler(MouseEvent.ANY, new DragEventHandler(this));

		// add elements to parent pane
		getChildren().addAll(slideContainer, slideShowControls, exitSlideShow);

		// add handlers start and pause
		onPause = new EventHandler<SlideShowEvent>() {
			@Override
			public void handle(SlideShowEvent arg0) {
				// nothing by default

			}
		};
		onStart = new EventHandler<SlideShowEvent>() {
			@Override
			public void handle(SlideShowEvent arg0) {
				// nothing by default

			}
		};
				
		addEventHandler(SlideShowEvent.SLIDESHOW_PAUSED, onPause);
		addEventHandler(SlideShowEvent.SLIDESHOW_STARTED, onStart);
	}

	/**
	 * @return the actual current Slide, the Slide object referenced by the
	 *         value of currentSlide.
	 */
	public Slide getCurrentSlide() {	
		return getCurrentPresentation().getSlide(currentSlide);
	}

	/**
	 * @return the actual current Presentation, the Presentation object
	 *         referenced by the value of currentPresentation.
	 */

	public Presentation getCurrentPresentation() {
		return presentationList.get(currentPresentation);
	}

	/**
	 * This throws an unchecked exception instead of returning null when the slide does not exist; always check with isTherePreviousSlide() before calling this.
	 * 
	 * @return slide before the current one, if there is one.
	 * @throws NoSuchSlideException thrown if no previous slide exists.
	 */
	public Slide getPreviousSlide() throws NoSuchSlideException {
		if (currentSlide > 0) {
			return getCurrentPresentation().getSlide(currentSlide - 1);
		} else {
			throw new NoSuchSlideException();
		}
	}

	/**
	 * This throws an unchecked exception instead of returning null when the slide does not exist; always check with isThereNextSlide() before calling this.
	 * 
	 * @return slide after the current one, if there is one.
	 * @throws NoSuchSlideException thrown if no next slide exists.
	 */
	public Slide getNextSlide() throws NoSuchSlideException {
		if (currentSlide < getCurrentPresentation().getSlides().size() - 1) {
			return getCurrentPresentation().getSlide(currentSlide + 1);
		} else {
			throw new NoSuchSlideException();
		}
	}

	/**
	 * This throws an unchecked exception instead of returning null when the presentation does not exist; always check with isTherePreviousPresentation() before calling this.
	 * 
	 * @return presentation before the current one, if there is one.
	 * @throws NoSuchPresentationException thrown if no previous presentation exists.
	 */
	public Presentation getPreviousPresentation() throws NoSuchPresentationException {
		if (currentPresentation > 0) {
			return presentationList.get(currentPresentation - 1);
		} else {
			throw new NoSuchPresentationException();
		}
	}

	/**
	 * This throws an unchecked exception instead of returning null when the presentation does not exist; always check with isThereNextPresentation() before calling this.
	 * 
	 * @return presentation after the current one, if there is one.
	 * @throws NoSuchPresentationException thrown if no next presentation exists.
	 */
	public Presentation getNextPresentation() throws NoSuchPresentationException {
		if (currentPresentation < presentationList.size() - 1) {
			return presentationList.get(currentPresentation + 1);
		} else {
			throw new NoSuchPresentationException();
		}
	}

	/**
	 * @return true if there is a next slide, false if otherwise.
	 */
	public boolean isThereNextSlide() {
		return (currentSlide < getCurrentPresentation().getSlides().size() - 1)
				&& getCurrentPresentation().getSlides().size() > 1;
	}

	/**
	 * @return true if there is a previous slide, false if otherwise.
	 */
	public boolean isTherePreviousSlide() {
		return (currentSlide > 0)
				&& getCurrentPresentation().getSlides().size() > 1;
	}

	/**
	 * @return true if there is a next presentation, false if otherwise.
	 */
	public boolean isThereNextPresentation() {
		return (currentPresentation < presentationList.size() - 1)
				&& presentationList.size() > 1;
	}

	/**
	 * @return true if there is a previous presentation, false if otherwise.
	 */
	public boolean isTherePreviousPresentation() {
		return (currentPresentation > 0) && presentationList.size() > 1;
	}

	/**
	 * Call this to resume the slideshow. It will become visible and
	 * start from the slide where it stopped.
	 */
	public void play() {
		// only play if it was paused
		if (handler.getSlideShowState() == SlideShowState.PAUSED) {
			// make slideshow visible
			fadeIn();

			// get focus
			requestFocus();

			// change state
			handler.setSlideShowState(SlideShowState.RESUMED);
			// notify thread monitor
			handler.notifyThread();

			// fire event
			fireEvent(new SlideShowEvent(SlideShowEvent.SLIDESHOW_STARTED));
		}
	}

	/**
	 * Call this when the slideshow pauses, but the app is still running and the
	 * user may want to see the slideshow again later.
	 */
	public void pause() {
		// only pause if it was playing
		if (handler.getSlideShowState() == SlideShowState.RESUMED) {
			// stop current slide content
			getCurrentSlide().stopMedia();
			// change state
			handler.setSlideShowState(SlideShowState.PAUSED);
			// asynchronous!
			handler.interrupt();
			// slideshow is invisible in pause mode
			fadeOut();

			// fire event
			fireEvent(new SlideShowEvent(SlideShowEvent.SLIDESHOW_PAUSED));
		}
	}

	/**
	 * Call this when the slideshow is no longer needed. This will kill the
	 * thread nicely and this slideshow will no longer be usable.
	 * 
	 * @deprecated the thread is now a daemon thread, so manually killing the slideshow
	 * 				is no longer necessary - it will automatically die when no non-daemon threads are left.
	 * 
	 */
	public void kill() {
		// no need to kill it if it's already dead
		if (handler.getSlideShowState() != SlideShowState.DEAD) {
			// redundancy, just in case
			setVisible(false);
			// user is closing the program, set state to dead
			handler.setSlideShowState(SlideShowState.DEAD);
			// interrupt!
			handler.interrupt();

			// now the thread dies safely
		}
	}

	/**
	 * Displays the next slide.
	 */
	public void goToNextSlide() {
		// OOB protection
		if (isThereNextSlide() && !busy) {
			// stop current media
			getCurrentSlide().stopMedia();

			// reset slides
			resetPresentations();

			// exit current slide
			getCurrentSlide().exitTo(
					(slideDirection == Orientation.HORIZONTAL) ? Direction.LEFT
							: Direction.UP);

			// increment slide index
			currentSlide++;

			// reset time
			handler.resetTime();

			// enter next slide
			getCurrentSlide().enterFrom(
					(slideDirection == Orientation.HORIZONTAL) ? Direction.RIGHT
							: Direction.DOWN);

			// refresh buttons
			slideShowControls.refreshButtons();
		}
	}

	/**
	 * Displays the previous slide.  
	 */
	public void goToPreviousSlide() {
		if (isTherePreviousSlide() && !busy) {
			// stop current media
			getCurrentSlide().stopMedia();

			// reset slides
			resetPresentations();

			// exit current slide
			getCurrentSlide().exitTo(
					(slideDirection == Orientation.HORIZONTAL) ? Direction.RIGHT
							: Direction.DOWN);

			// decrement slide index, or wrap
			currentSlide--;

			// reset time
			handler.resetTime();

			// enter previous slide
			getCurrentSlide().enterFrom(
					(slideDirection == Orientation.HORIZONTAL) ? Direction.LEFT
							: Direction.UP);

			// refresh buttons
			slideShowControls.refreshButtons();
		}
	}

	/**
	 * Scrolls to the specified slide. If the provided index is the current
	 * slide, does nothing.
	 * 
	 * @param index the slide to scroll to.
	 */
	public void goToSlide(final int index) {
		// this only happens if we're not already on the given index AND we're
		// not busy AND the given index is within bounds
		if ((index != currentSlide) && (!busy)
				&& (index < getCurrentPresentation().getSlides().size())
				&& (index >= 0)) {
			// we are now busy
			busy = true;

			// should be threaded
			Thread t = new Thread(new Runnable() {

				@Override
				public void run() {
					// stop current media
					getCurrentSlide().stopMedia();
					// lets do this - first pause the thread so it doesn't try
					// to play stuff
					handler.setSlideShowState(SlideShowState.PAUSED);
					handler.interrupt();

					while (currentSlide != index) {
						// have to go backwards
						if (currentSlide > index) {
							// exit current slide
							getCurrentSlide()
							.exitTo((slideDirection == Orientation.HORIZONTAL) ? Direction.RIGHT
									: Direction.DOWN);

							// decrement slide index
							currentSlide--;

							// enter previous slide
							getCurrentSlide()
							.enterFrom(
									(slideDirection == Orientation.HORIZONTAL) ? Direction.LEFT
											: Direction.UP);
						}
						// have to go forward
						else {
							// exit current slide
							getCurrentSlide()
							.exitTo((slideDirection == Orientation.HORIZONTAL) ? Direction.LEFT
									: Direction.UP);

							// increment slide index
							currentSlide++;

							// enter next slide
							getCurrentSlide()
							.enterFrom(
									(slideDirection == Orientation.HORIZONTAL) ? Direction.RIGHT
											: Direction.DOWN);
						}
						// wait before starting the next one
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}

					// done, resume handling
					handler.setSlideShowState(SlideShowState.RESUMED);
					handler.notifyThread();

					// refresh buttons
					slideShowControls.refreshButtons();

					// we are no longer busy
					busy = false;

				}
			});

			// do it
			t.start();
		}
	}

	/**
	 * Displays the next presentation
	 * 
	 */
	public void goToNextPresentation() {
		// only if not busy AND if there is a presentation to go to OR we are
		// wrapping
		if (isThereNextPresentation() && !busy) {

			// stop current media
			getCurrentSlide().stopMedia();

			// reset slides
			resetSlides();

			// exit current slide
			getCurrentSlide().exitTo(
					(slideDirection == Orientation.HORIZONTAL) ? Direction.UP
							: Direction.LEFT);

			// increment or wrap index, reset currentSlide
			currentPresentation++;
			currentSlide = 0;

			// sort out handler
			handler.resetTime();

			// move in next slide
			getCurrentSlide().enterFrom(
					(slideDirection == Orientation.HORIZONTAL) ? Direction.DOWN
							: Direction.RIGHT);

			// refresh buttons
			slideShowControls.refreshButtons();
		}
	}

	/**
	 * Displays the previous presentation.
	 * 
	 */
	public void goToPreviousPresentation() {
		// only if not busy AND if there is a presentation to go to OR we are
		// wrapping
		if (isTherePreviousPresentation() && !busy) {
			// stop current media
			getCurrentSlide().stopMedia();

			// reset slides
			resetSlides();

			// exit current slide
			getCurrentSlide().exitTo(
					(slideDirection == Orientation.HORIZONTAL) ? Direction.DOWN
							: Direction.RIGHT);

			// decrement or wrap index, reset currentSlide
			currentPresentation--;
			currentSlide = 0;

			// sort out handler
			handler.resetTime();

			// move in next slide
			getCurrentSlide().enterFrom(
					(slideDirection == Orientation.HORIZONTAL) ? Direction.UP
							: Direction.LEFT);

			// refresh buttons
			slideShowControls.refreshButtons();
		}
	}

	/**
	 * Go right, whatever that means - it depends on the configuration.
	 * 
	 * @param reset true causes the slide to re-centre before entering, false
	 *            makes it enter from its current position.
	 */
	public void goRight() {
		if (slideDirection == Orientation.HORIZONTAL) {
			goToNextSlide();
		} else if (slideDirection == Orientation.VERTICAL) {
			goToNextPresentation();
		}
	}

	/**
	 * Go left, whatever that means - it depends on the configuration.
	 * 
	 * @param reset true causes the slide to re-centre before entering, false
	 *            makes it enter from its current position.
	 */
	public void goLeft() {
		if (slideDirection == Orientation.HORIZONTAL) {
			goToPreviousSlide();
		} else if (slideDirection == Orientation.VERTICAL) {
			goToPreviousPresentation();
		}
	}

	/**
	 * Go down, whatever that means - it depends on the configuration.
	 * 
	 * @param reset true causes the slide to re-centre before entering, false
	 *            makes it enter from its current position.
	 */
	public void goDown() {
		if (slideDirection == Orientation.HORIZONTAL) {
			goToNextPresentation();
		} else if (slideDirection == Orientation.VERTICAL) {
			goToNextSlide();
		}
	}

	/**
	 * Go up, whatever that means - it depends on the configuration.
	 * 
	 * @param reset true causes the slide to re-centre before entering, false
	 *            makes it enter from its current position.
	 */
	public void goUp() {
		if (slideDirection == Orientation.HORIZONTAL) {
			goToPreviousPresentation();
		} else if (slideDirection == Orientation.VERTICAL) {
			goToPreviousSlide();
		}
	}

	/**
	 * fades in the slideshow.
	 */
	private void fadeIn() {
		// fade in
		FadeTransition ft = new FadeTransition(Duration.millis(200), this);
		ft.setFromValue(0.0);
		ft.setToValue(1.0);

		// make visible
		setVisible(true);

		// go!
		ft.play();
	}

	/**
	 * fades out the slideshow.
	 */
	private void fadeOut() {
		// fade out
		FadeTransition ft = new FadeTransition(Duration.millis(200), this);
		ft.setFromValue(1.0);
		ft.setToValue(0.0);

		// invisible when finished
		ft.setOnFinished(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				setVisible(false);
			}
		});

		// go!
		ft.play();
	}

	/**
	 * This method sets the previous and next slides invisible, in case a transition happens while click and drag is being done.
	 */
	private void resetSlides(){
		if(isThereNextSlide()){
			getNextSlide().setVisible(false);
		}
		if(isTherePreviousSlide()){
			getPreviousSlide().setVisible(false);
		}

	}

	/**
	 * This method sets previous and next presentations to invisible, in case a transition happens while click and dragging.
	 */
	private void resetPresentations(){
		if(isThereNextPresentation()){
			getNextPresentation().getSlide(0).setVisible(false);
		}
		if(isTherePreviousPresentation()){
			getPreviousPresentation().getSlide(0).setVisible(false);
		}
	}

}
