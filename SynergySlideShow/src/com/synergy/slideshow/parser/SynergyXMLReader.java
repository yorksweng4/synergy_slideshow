/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow.parser;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyDoubleWrapper;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.geometry.VPos;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.EllipseBuilder;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextBuilder;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.synergy.slideshow.builder.AlbumBuilder;
import com.synergy.slideshow.builder.AudioBuilder;
import com.synergy.slideshow.builder.ImageBuilder;
import com.synergy.slideshow.builder.PolyBuilder;
import com.synergy.slideshow.builder.VideoBuilder;
import com.synergy.slideshow.presentable.Presentable;
import com.synergy.slideshow.presentable.Presentation;
import com.synergy.slideshow.presentable.PresentationList;
import com.synergy.slideshow.slide.Instruction;
import com.synergy.slideshow.slide.Slide;

enum ProcessingElement {
	NONE, TEXT, POLYGON, DIMX, DIMY, AUTHOR, TITLE, DEFAULT, CONTENT, LIST;
};

/**
 * 
 * Standard XML parser, this will parse basic presentation XMLs
 * and Synergy presentation lists.
 * 
 * This parser complies with PWS v5.2.
 * 
 * @author Matt, Eddy, Ben, Rory
 *
 */
public class SynergyXMLReader extends DefaultHandler {

	// locator fields
	private Locator loc1;
	private int lineTotal = 1;
	private String currentFile;
	
	// read only properties so user can track progress
	private ReadOnlyDoubleWrapper progress;
	private ReadOnlyStringWrapper file;

	// processing element fields, need two because this parser can handle the default section of PWS as well
	private ProcessingElement currentElement = ProcessingElement.NONE;
	private ProcessingElement currentSection = ProcessingElement.NONE;

	// presentables which are populated and returned
	private PresentationList presentationList;
	private Presentation currentPresentation;

	// these are all used by the parser as it parses elements
	private int dimX;
	private int dimY;
	private String author;
	private String title;
	private StringBuilder stringBuild;
	private Slide slide;
	private Text text;
	private PolyBuilder poly;
	private AlbumBuilder albumBuild;
	private ArrayList<Slide> slides;
	private ArrayList<Double> defaultPoints;

	// defaults
	private HashMap<String, String> defaultValues;
	private String defaultText;

	/**
	 * Constructs a SynergyXMLReader to parse PWS-style and PresentationList XML files.
	 * 
	 */
	public SynergyXMLReader() {
		super();

		// generate hard-coded default values
		populateDefault();

		// prepare progress
		progress = new ReadOnlyDoubleWrapper(0.0);
		file = new ReadOnlyStringWrapper("");
	}

	/**
	 * Call this method to parse a file. The parser will populate the data structures according to XML content.
	 * The data can be retrieved once parsed with getPresentable().
	 * 
	 * @param inputFile the URL or local address of the file
	 * @throws SAXException if a provided file does not exist or is not reachable.
	 * This exception can be caught to warn the user of which file it is.
	 * @throws SAXParseException warns the user of an XML syntax error.
	 * This exception contains the line and column number of the error. Explicitly catch this before the
	 * SAXException to deal with XML syntax errors.
	 */
	public void parseFile(String inputFile) throws SAXException{

		// create a new parser
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			currentFile = inputFile;

			// now parse the file using this class as the handler
			saxParser.parse(inputFile, this);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (IOException e){
			throw new SAXException(e);
		}
	}

	@Override
	public void setDocumentLocator(Locator l){
		// use our locator
		this.loc1 = l;
	}

	/**
	 * @return current line being parsed.
	 */
	public int getLine(){
		return loc1.getLineNumber();
	}

	/**
	 * Indicates a document has been found and is being read.
	 */
	public void startDocument() throws SAXException {
		System.out.println("Starting to process document.");
	}

	/**
	 * Called by the parser when it encounters a start element within the xml, 
	 * this then calls methods to assign data to slides
	 */
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		String elementName = localName;
		if ("".equals(elementName)) {
			elementName = qName;
		}
		System.out.println("\tFound the start of an element (" + elementName
				+ ") ...");

		// Here we deal with all possible tags in both presentation lists and single PWS presentations
		if (elementName.equalsIgnoreCase("PresentationList")) {
			//Initialise variables with defaults
			String id = "Default PB ID";
			double w = 800.0, h = 800.0;
			//Scan through attributes. If a relevant one exists and it's not empty, overwrite the default.
			for(int i = 0; i < attributes.getLength(); i++){
				if(attributes.getLocalName(i).equalsIgnoreCase("ID")){
					if(!attributes.getValue(i).isEmpty()){
						id = attributes.getValue(i);
					}
				}
				if(attributes.getLocalName(i).equalsIgnoreCase("width")){
					if(!attributes.getValue(i).isEmpty()){
						w = Double.parseDouble(attributes.getValue(i));
					}
				}
				if(attributes.getLocalName(i).equalsIgnoreCase("height")){
					if(!attributes.getValue(i).isEmpty()){
						h = Double.parseDouble(attributes.getValue(i));
					}
				}
			}
			//Make pinboard
			presentationList = new PresentationList(id, w, h);

		} else if (elementName.equalsIgnoreCase("SubPresentation")) {
			//If the presentation URL isn't present, use this default slideshow
			String url = "http://benshouse.crabdance.com/xml/POI/default/1Default.xml";

			if(attributes.getLength() == 1 && !attributes.getValue(0).isEmpty()){
				url = attributes.getValue(0);
			}

			currentFile = url;

			//All IO exceptions are caught here. For now we'll just whack up the default slideshow
			try{

				SAXParserFactory factory = SAXParserFactory.newInstance();
				SAXParser saxParser = factory.newSAXParser();

				// now parse the file using this class as the handler
				saxParser.parse(url, this);

			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (IOException e) {
				throw new SAXException(e);
			}

		} else if (elementName.equalsIgnoreCase("Document") || elementName.equalsIgnoreCase("Presentation")) {
			// get total number of lines
			try {
				InputStream is;
				if(!currentFile.startsWith("http://")){
					currentFile = "file:///".concat(currentFile);
				}
				is = new URL(currentFile.replace(" ", "%20")).openStream();
				byte[] c = new byte[1024];
				int count = 0;
				int readChars = 0;
				boolean empty = true;
				while ((readChars = is.read(c)) != -1) {
					empty = false;
					for (int i = 0; i < readChars; ++i) {
						if (c[i] == '\n') {
							++count;
						}
					}
				}
				lineTotal = (count == 0 && !empty) ? 1 : count;
				is.close();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} 

			file.setValue(currentFile);
			progress.set(0.0);
		} else if (elementName.equalsIgnoreCase("Header")) {

		} else if (elementName.equalsIgnoreCase("Title")) {
			currentElement = ProcessingElement.TITLE;
		} else if (elementName.equalsIgnoreCase("Author")) {
			currentElement = ProcessingElement.AUTHOR;
		} else if (elementName.equalsIgnoreCase("DimensionsX")) {
			currentElement = ProcessingElement.DIMX;
		} else if (elementName.equalsIgnoreCase("DimensionsY")) {
			currentElement = ProcessingElement.DIMY;
		} else if (elementName.equalsIgnoreCase("Default")) {
			currentSection = ProcessingElement.DEFAULT;
		} else if (elementName.equalsIgnoreCase("Content")) {
			currentSection = ProcessingElement.CONTENT;
			slides = new ArrayList<Slide>();
		} else if (elementName.equalsIgnoreCase("Slide")) {

			// parse content if the current section is content
			if(currentSection == ProcessingElement.CONTENT){
				// Defaults
				String id = defaultValues.get("slideid"), duration = defaultValues.get("slideduration"), backcolour = defaultValues.get("slidebgcolor");

				// Scan through attributes. If a relevant one exists and it's not empty, overwrite the default.
				for(int i = 0; i < attributes.getLength(); i++){
					if(attributes.getLocalName(i).equalsIgnoreCase("ID")){
						if(!attributes.getValue(i).isEmpty()){
							id = attributes.getValue(i);
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("BgColor")){
						if(!attributes.getValue(i).isEmpty()){
							backcolour = attributes.getValue(i);
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("Duration")){
						if(!attributes.getValue(i).isEmpty()){
							duration = attributes.getValue(i);
						}
					}
				}

				slide = new Slide(id, Integer.parseInt(duration), dimX, dimY);
				slide.setStyle("-fx-background-color: #"+backcolour);

			} else if (currentSection == ProcessingElement.DEFAULT){
				// replace defaults with any user-specified values given
				for(int i = 0; i < attributes.getLength(); i++){
					if(attributes.getValue(i) != null && !attributes.getValue(i).isEmpty()){
						defaultValues.put("slide" + attributes.getLocalName(i).toLowerCase(), attributes.getValue(i));
					}
				}
			}

		} else if (elementName.equalsIgnoreCase("Text")) {
			//parse text to text handler 
			currentElement = ProcessingElement.TEXT;

			if(currentSection == ProcessingElement.CONTENT){

				// need this to build the string itself
				stringBuild = new StringBuilder();

				int st, et;
				try {
					st = Integer.parseInt(defaultValues.get("textstarttime"));
				} catch(NumberFormatException nfe){
					st = 0;
				}
				try {
					et = Integer.parseInt(defaultValues.get("textduration"));
				} catch(NumberFormatException nfe){
					et = 0;
				}

				//parse text information, such as colour, font etc.
				//Set up default values, these will be overwritten if they exist in xml
				String id, x, y, fill, underline, font, bold, italic, size;
				id = defaultValues.get("textid");
				x = defaultValues.get("textxpos");
				y = defaultValues.get("textypos");
				fill = defaultValues.get("textfontcolor");
				underline = defaultValues.get("textunderline");
				font = defaultValues.get("textfontface");
				bold = defaultValues.get("textbold");
				italic = defaultValues.get("textitalic");
				size = defaultValues.get("textfontsize");

				//Scan through attributes. If a relevant one exists and it's not empty, overwrite the default.
				for(int i = 0; i < attributes.getLength(); i++){
					if(attributes.getLocalName(i).equalsIgnoreCase("ID")){
						if(!attributes.getValue(i).isEmpty()){
							id = attributes.getValue(i);
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("xPos")){
						if(!attributes.getValue(i).isEmpty()){
							x = attributes.getValue(i);
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("yPos")){
						if(!attributes.getValue(i).isEmpty()){
							y = attributes.getValue(i);
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("FontFace")){
						if(!attributes.getValue(i).isEmpty()){
							font = attributes.getValue(i);
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("FontSize")){
						if(!attributes.getValue(i).isEmpty()){
							size = attributes.getValue(i);
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("FontColor")){
						if(!attributes.getValue(i).isEmpty()){
							fill = attributes.getValue(i);
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("Underline")){
						if(!attributes.getValue(i).isEmpty()){
							underline = attributes.getValue(i);
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("Bold")){
						if(!attributes.getValue(i).isEmpty()){
							bold = attributes.getValue(i);
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("Italic")){
						if(!attributes.getValue(i).isEmpty()){
							italic = attributes.getValue(i);
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("StartTime")){
						if(!attributes.getValue(i).isEmpty()){
							try{
								st = Integer.parseInt(attributes.getValue(i));
							} catch(NumberFormatException e){
								st = 0;
							}
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("Duration")){
						if(!attributes.getValue(i).isEmpty()){
							try{
								et = Integer.parseInt(attributes.getValue(i));
							} catch(NumberFormatException e){
								et = 0;
							}
						}
					}
				}			

				text = TextBuilder.create()
						.id(id)
						.x(Double.parseDouble(x))
						.y(Double.parseDouble(y))
						.underline(Boolean.parseBoolean(underline))
						.font(Font.font(font,
								Boolean.parseBoolean(bold) ? FontWeight.BOLD : FontWeight.NORMAL,
										Boolean.parseBoolean(italic) ? FontPosture.ITALIC : FontPosture.REGULAR,
												Double.parseDouble(size)))
												.build();

				text.setTextOrigin(VPos.TOP);

				// Exception catch for illegal colours
				try {
					text.setFill(Color.web(fill));
				} catch (IllegalArgumentException e) {
					text.setFill(Color.web("000000"));
				}
				slide.addInstruction(new Instruction(text, st, et));

			}  else if (currentSection == ProcessingElement.DEFAULT){
				// replace defaults with any user-specified values given
				for(int i = 0; i < attributes.getLength(); i++){
					if(attributes.getValue(i) != null && !attributes.getValue(i).isEmpty()){
						defaultValues.put("text" + attributes.getLocalName(i).toLowerCase(), attributes.getValue(i));
					}
				}

				stringBuild = new StringBuilder();	
			}

		} else if (elementName.equalsIgnoreCase("Polygon")) {
			if(currentSection == ProcessingElement.CONTENT){
				//parse polygon Id and duration and instantiate a new polyBuilder.
				poly = new PolyBuilder(attributes, defaultValues);
			}  else if (currentSection == ProcessingElement.DEFAULT){
				// replace defaults with any user-specified values given
				for(int i = 0; i < attributes.getLength(); i++){
					if(attributes.getValue(i) != null && !attributes.getValue(i).isEmpty()){
						defaultValues.put("polygon" + attributes.getLocalName(i).toLowerCase(), attributes.getValue(i));
					}
				}
			}

		} else if (elementName.equalsIgnoreCase("Oval")) {
			if(currentSection == ProcessingElement.CONTENT){
				int st = Integer.parseInt(defaultValues.get("ovalstarttime")),
						et = Integer.parseInt(defaultValues.get("ovalduration"));

				double xPos = Double.parseDouble(defaultValues.get("ovalxpos")), yPos = Double.parseDouble(defaultValues.get("ovalypos")), xRad = Double.parseDouble(defaultValues.get("ovalxsize")), yRad = Double.parseDouble(defaultValues.get("ovalysize")), strokeWidth = Double.parseDouble(defaultValues.get("ovallinethickness"));
				String id = defaultValues.get("ovalid"), strokeColour = defaultValues.get("ovallinecolor"), fillColour = defaultValues.get("ovalfillcolor");
				Ellipse oval;

				//parse all oval information incl. color, size, position et.c
				//Scan through attributes. If a relevant one exists and it's not empty, overwrite the default.
				for(int i = 0; i < attributes.getLength(); i++){
					if(attributes.getLocalName(i).equalsIgnoreCase("ID")){
						if(!attributes.getValue(i).isEmpty()){
							id = attributes.getValue(i);
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("xPos")){
						if(!attributes.getValue(i).isEmpty()){
							xPos = Double.parseDouble(attributes.getValue(i));
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("yPos")){
						if(!attributes.getValue(i).isEmpty()){
							yPos = Double.parseDouble(attributes.getValue(i));
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("xSize")){
						if(!attributes.getValue(i).isEmpty()){
							xRad = Double.parseDouble(attributes.getValue(i));
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("ySize")){
						if(!attributes.getValue(i).isEmpty()){
							yRad = Double.parseDouble(attributes.getValue(i));
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("lineThickness")){
						if(!attributes.getValue(i).isEmpty()){
							strokeWidth = Double.parseDouble(attributes.getValue(i));
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("lineColour")){
						if(!attributes.getValue(i).isEmpty()){
							strokeColour = attributes.getValue(i);
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("fillColour")){
						if(!attributes.getValue(i).isEmpty()){
							fillColour = attributes.getValue(i);
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("StartTime")){
						if(!attributes.getValue(i).isEmpty()){
							st = Integer.parseInt(attributes.getValue(i));
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("Duration")){
						if(!attributes.getValue(i).isEmpty()){
							et = Integer.parseInt(attributes.getValue(i));
						}
					}
				}			

				oval = EllipseBuilder.create()
						.id(id)
						.centerX(xPos+xRad)
						.centerY(yPos+yRad)
						.radiusX(xRad)
						.radiusY(yRad)
						.strokeWidth(strokeWidth)
						.stroke(Color.web(strokeColour))
						.fill(Color.web(fillColour))
						.build();

				slide.addContent(oval, new Instruction(oval, st, et));

			} else if (currentSection == ProcessingElement.DEFAULT){
				// replace defaults with any user-specified values given
				for(int i = 0; i < attributes.getLength(); i++){
					if(attributes.getValue(i) != null && !attributes.getValue(i).isEmpty()){
						defaultValues.put("oval" + attributes.getLocalName(i).toLowerCase(), attributes.getValue(i));
					}
				}
			}
		} else if (elementName.equalsIgnoreCase("Image")) {
			if(currentSection == ProcessingElement.CONTENT){

				int st = Integer.parseInt(defaultValues.get("imagestarttime")), et = Integer.parseInt(defaultValues.get("imageduration"));

				//parse video information and file location						
				ImageBuilder imageBuild = new ImageBuilder(attributes, defaultValues);

				//Scan through attributes. If a relevant one exists and it's not empty, overwrite the default.
				for(int i = 0; i < attributes.getLength(); i++){
					if(attributes.getLocalName(i).equalsIgnoreCase("StartTime")){
						if(!attributes.getValue(i).isEmpty()){
							st = Integer.parseInt(attributes.getValue(i));
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("Duration")){
						if(!attributes.getValue(i).isEmpty()){
							et = Integer.parseInt(attributes.getValue(i));
						}
					}
				}

				slide.addContent(imageBuild.getBuilt(), new Instruction(imageBuild.getBuilt(), st, et));

			} else if (currentSection == ProcessingElement.DEFAULT){
				// replace defaults with any user-specified values given
				for(int i = 0; i < attributes.getLength(); i++){
					if(attributes.getValue(i) != null && !attributes.getValue(i).isEmpty()){
						defaultValues.put("image" + attributes.getLocalName(i).toLowerCase(), attributes.getValue(i));
					}
				}
			}
		} else if (elementName.equalsIgnoreCase("Video")) {
			if(currentSection == ProcessingElement.CONTENT){
				int st = Integer.parseInt(defaultValues.get("videostarttime")), et = Integer.parseInt(defaultValues.get("videoduration"));

				//parse video information and file location	
				VideoBuilder vidBuild = new VideoBuilder(attributes, defaultValues);

				//Scan through attributes. If a relevant one exists and it's not empty, overwrite the default.
				for(int i = 0; i < attributes.getLength(); i++){
					if(attributes.getLocalName(i).equalsIgnoreCase("StartTime")){
						if(!attributes.getValue(i).isEmpty()){
							st = Integer.parseInt(attributes.getValue(i));
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("Duration")){
						if(!attributes.getValue(i).isEmpty()){
							et = Integer.parseInt(attributes.getValue(i));
						}
					}
				}

				//Give it to a slide with or without inlineControls 
				slide.addContent(vidBuild.getBuilt(), new Instruction(vidBuild.getBuilt(), st, et));
			} else if (currentSection == ProcessingElement.DEFAULT){
				// replace defaults with any user-specified values given
				for(int i = 0; i < attributes.getLength(); i++){
					if(attributes.getValue(i) != null && !attributes.getValue(i).isEmpty()){
						defaultValues.put("video" + attributes.getLocalName(i).toLowerCase(), attributes.getValue(i));
					}
				}
			}
		} else if (elementName.equalsIgnoreCase("Audio")) {
			if(currentSection == ProcessingElement.CONTENT){
				int st = Integer.parseInt(defaultValues.get("audiostarttime")), et = Integer.parseInt(defaultValues.get("audioduration"));

				//parse audio information and file location	
				AudioBuilder audBuild = new AudioBuilder(attributes, defaultValues);

				//Scan through attributes. If a relevant one exists and it's not empty, overwrite the default.
				for(int i = 0; i < attributes.getLength(); i++){
					if(attributes.getLocalName(i).equalsIgnoreCase("StartTime")){
						if(!attributes.getValue(i).isEmpty()){
							st = Integer.parseInt(attributes.getValue(i));
						}
					}
					if(attributes.getLocalName(i).equalsIgnoreCase("Duration")){
						if(!attributes.getValue(i).isEmpty()){
							et = Integer.parseInt(attributes.getValue(i));
						}
					}
				}

				//Give it to a slide with or without inlineControls 
				slide.addContent(audBuild.getBuilt(), new Instruction(audBuild.getBuilt(), st, et));
			} else if (currentSection == ProcessingElement.DEFAULT){
				// replace defaults with any user-specified values given
				for(int i = 0; i < attributes.getLength(); i++){
					if(attributes.getValue(i) != null && !attributes.getValue(i).isEmpty()){
						defaultValues.put("audio" + attributes.getLocalName(i).toLowerCase(), attributes.getValue(i));
					}
				}
			}
		} else if (elementName.equalsIgnoreCase("Album")) {
			// find out whether this is a synergy album
			for(int i = 0; i < attributes.getLength(); i++){
				if((attributes.getLocalName(i).equalsIgnoreCase("Compatibility")) && (attributes.getValue(i).equalsIgnoreCase("sy"))){
					// Create new instance of AlbumBuilder to construct new Album
					albumBuild = new AlbumBuilder(attributes, defaultValues);
					break;
				}
			}

		} else if (elementName.equalsIgnoreCase("AlbumImage")) {
			// The image is sent to the album builder to be added to Album's imageContainer - but only if albumbuild isn't null
			if(albumBuild != null){
				albumBuild.addImage(attributes);
			}
		} else if (elementName.equalsIgnoreCase("Point")) {

			String xPos = "";
			String yPos = "";

			//Scan through attributes. If a relevant one exists and it's not empty, overwrite the default.
			for(int i = 0; i < attributes.getLength(); i++){
				if(attributes.getLocalName(i).equalsIgnoreCase("xPos")){
					if(!attributes.getValue(i).isEmpty()){
						xPos = attributes.getValue(i);
					}
				}
				if(attributes.getLocalName(i).equalsIgnoreCase("yPos")){
					if(!attributes.getValue(i).isEmpty()){
						yPos = attributes.getValue(i);
					}
				}
			}

			//If one of the coord's is missing, dont add the point.
			if(!xPos.isEmpty() && !yPos.isEmpty()){
				if(currentSection == ProcessingElement.CONTENT){
					//add points by calling the PolyBuilder addpoints method.
					poly.addPoint(Double.parseDouble(xPos), Double.parseDouble(yPos));
				} else if(currentSection == ProcessingElement.DEFAULT) {
					defaultPoints.add(Double.parseDouble(xPos));
					defaultPoints.add(Double.parseDouble(yPos));
				}
			}

		} else if (elementName.equalsIgnoreCase("Break")) {
			//append text already assigned to a slide. just adds a new line
			stringBuild.append("\n");
		}
	}

	/**
	 * Called by the parser when it encounters characters in the main body of an
	 * element.
	 */
	public void characters(char[] ch, int start, int length) throws SAXException {
		System.out.println("\tFound some characters ...");

		// update progress
		progress.set(((double) getLine()/lineTotal) >= -1 ? (double) getLine()/lineTotal : -1 );

		switch (currentElement) {
		case TEXT:
			//when text content is found append it to stringBuild
			stringBuild.append(new String(ch, start, length).trim());
			break;
		case TITLE:
			title = new String(ch, start, length);
			break;
		case AUTHOR:
			author = new String(ch, start, length);
			break;
		case DIMX:
			dimX = Integer.parseInt(new String(ch, start, length).trim());
			break;
		case DIMY:
			dimY = Integer.parseInt(new String(ch, start, length).trim());
			break;
		default:
			break;
		}
	}

	/**
	 * Called by the parser when it encounters any end element tag.
	 */
	public void endElement(String uri, String localName, String qName) throws SAXException {
		// sort out element name if (no) namespace in use
		String elementName = localName;
		if ("".equals(elementName)) {
			elementName = qName;
		}
		System.out.println("\tFound the end of an element (" + elementName
				+ ") ...");

		// here we deal with all possible tags being closed
		if (elementName.equalsIgnoreCase("Slide")) {
			if(currentSection == ProcessingElement.CONTENT){
				slides.add(slide);
				slide = null;
			} else if(currentSection == ProcessingElement.CONTENT){
				slide = null;
			}
		} else if (elementName.equalsIgnoreCase("Text")) {

			if(currentSection == ProcessingElement.CONTENT){
				currentElement = ProcessingElement.NONE;

				String builtText = stringBuild.toString();

				if(builtText.isEmpty() || builtText == null){
					builtText = defaultText;
				}

				text.setText(builtText);

				slide.getChildren().add(text);

			} else if (currentSection == ProcessingElement.DEFAULT){
				currentElement = ProcessingElement.NONE;

				String builtText = stringBuild.toString();

				if(!builtText.isEmpty() && builtText != null){
					defaultText = builtText;
				}
			}

		} else if (elementName.equalsIgnoreCase("Title")) {
			currentElement = ProcessingElement.NONE;
		} else if (elementName.equalsIgnoreCase("Author")) {
			currentElement = ProcessingElement.NONE;
		} else if (elementName.equalsIgnoreCase("DimensionsX")) {
			currentElement = ProcessingElement.NONE;
		} else if (elementName.equalsIgnoreCase("DimensionsY")) {
			currentElement = ProcessingElement.NONE;
		} else if (elementName.equalsIgnoreCase("Polygon")) {
			if(currentSection == ProcessingElement.CONTENT){
				if(!poly.areTherePoints()){
					poly.addPoint(defaultPoints);
				}
				slide.addContent(poly.getBuilt(), poly.getInstruction());
			} else if(currentSection == ProcessingElement.DEFAULT){

			}

		} else if (elementName.equalsIgnoreCase("Album")) {
			if(albumBuild != null){
				// A command to ready the albums for initial viewing
				albumBuild.finaliseAlbum();
				// Add album to the slide
				slide.addContent(albumBuild.getBuilt(), albumBuild.getInstruction());
				// reset album builder
				albumBuild = null;
			}
		} else if (elementName.equalsIgnoreCase("Default")) {
			currentSection = ProcessingElement.NONE;
		} else if (elementName.equalsIgnoreCase("Content")) {
			currentSection = ProcessingElement.NONE;
		} else if (elementName.equalsIgnoreCase("Document") || elementName.equalsIgnoreCase("Presentation")) {
			// if we're here, it means we're done
			progress.set(1.0);

			if(presentationList != null){
				presentationList.addPresentation(new Presentation(slides, title, author, dimX, dimY));
			} else {
				currentPresentation = new Presentation(slides, title, author, dimX, dimY);
			}
		} 
	}

	/**
	 * Called by the parser when it encounters the end of the XML file.
	 */
	public void endDocument() throws SAXException {
		System.out.println("Finished processing document.");
	}

	/**
	 * Call this method to get the parsed Presentable object, whatever it may be.
	 * 
	 * @return a Presentable that has been parsed from the XML.
	 */
	public Presentable getPresentable(){
		return (presentationList == null) ? currentPresentation : presentationList;
	}


	/**
	 * @return the double property representing the progress, between 0 and 1.
	 */
	public ReadOnlyDoubleProperty progressProperty(){
		return progress.getReadOnlyProperty();
	}

	/**
	 * @return the String property containing the name and path of the file being parsed.
	 */
	public ReadOnlyStringProperty fileNameProperty(){
		return file.getReadOnlyProperty();
	}

	/**
	 * This method generates the hard-coded default values, for safety.
	 */
	private void populateDefault() {

		// instantiate default holders
		defaultValues = new HashMap<String, String>();
		defaultText = "Default Text";
		defaultPoints = new ArrayList<Double>();

		// slide defaults
		defaultValues.put("slideid", "default"); 
		defaultValues.put("slidebgcolor", "FFFFFF"); 
		defaultValues.put("slideduration", "0");

		// text defaults
		defaultValues.put("textid", "default");
		defaultValues.put("textxpos", "50");
		defaultValues.put("textypos", "100");
		defaultValues.put("textfontcolor", "000000");
		defaultValues.put("textunderline", "false");
		defaultValues.put("textfontface", "Arial");
		defaultValues.put("textbold", "false");
		defaultValues.put("textitalic", "false");
		defaultValues.put("textfontsize", "20");
		defaultValues.put("textstarttime", "0");
		defaultValues.put("textduration", "0");

		// polygon defaults
		defaultValues.put("polygonid", "default");
		defaultValues.put("polygonlinethickness", "1");
		defaultValues.put("polygonlinecolor", "000000");
		defaultValues.put("polygonfillcolor", "FFFFFF");
		defaultValues.put("polygonstarttime", "0");
		defaultValues.put("polygonduration", "0");

		// oval defaults
		defaultValues.put("ovalid", "default");
		defaultValues.put("ovalxpos", "0");
		defaultValues.put("ovalypos", "0");
		defaultValues.put("ovalxsize", "10");
		defaultValues.put("ovalysize", "10");
		defaultValues.put("ovallinethickness", "1");
		defaultValues.put("ovallinecolor", "000000");
		defaultValues.put("ovalfillcolor", "FFFFFF");
		defaultValues.put("ovalstarttime", "0");
		defaultValues.put("ovalduration", "0");

		// image defaults
		defaultValues.put("imageid", "default");
		defaultValues.put("imagefilelocation", "http://benshouse.crabdance.com/xml/photos/imagenotfound.png");
		defaultValues.put("imagexpos", "0");
		defaultValues.put("imageypos", "0");
		defaultValues.put("imagexsize", "");
		defaultValues.put("imageysize", "");
		defaultValues.put("imagestarttime", "0");
		defaultValues.put("imageduration", "0");

		// video defaults
		defaultValues.put("videoid", "default");
		defaultValues.put("videofilelocation", "http://download.oracle.com/otndocs/products/javafx/oow2010-2.flv");
		defaultValues.put("videoxpos", "0");
		defaultValues.put("videoypos", "0");
		defaultValues.put("videoxsize", "");
		defaultValues.put("videoysize", "");
		defaultValues.put("videoinlinecontrol", "true");
		defaultValues.put("videostarttime", "0");
		defaultValues.put("videoduration", "0");

		// audio defaults
		defaultValues.put("audioid", "default");
		defaultValues.put("audiofilelocation", "http://princezze.free.fr/sounds/saw_cut.wav");
		defaultValues.put("audioxpos", "0");
		defaultValues.put("audioypos", "0");
		defaultValues.put("audioxsize", "128");
		defaultValues.put("audioysize", "128");
		defaultValues.put("audioinlinecontrol", "true");
		defaultValues.put("audiostarttime", "0");
		defaultValues.put("audioduration", "0");

		// album defaults
		defaultValues.put("albumid", "default");
		defaultValues.put("albumxpos", "0");
		defaultValues.put("albumypos", "0");
		defaultValues.put("albumxsize", "128");
		defaultValues.put("albumysize", "128");
		defaultValues.put("albumstarttime", "0");
		defaultValues.put("albumduration", "0");

	}

}
