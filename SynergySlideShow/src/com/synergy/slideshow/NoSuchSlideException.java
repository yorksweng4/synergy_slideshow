/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow;

/**
 * 
 * This exception is thrown when getNextSlide() or getPreviousSlide()
 * have nothing to return. Always check with isThereNextSlide() and isTherePreviousSlide() to avoid
 * these exceptions.
 * 
 * @author Eddy
 *
 */
public class NoSuchSlideException extends RuntimeException {

	private static final long serialVersionUID = -1226618440106629730L;

}
