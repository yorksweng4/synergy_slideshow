/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow;

/**
 * 
 * This exception is thrown when getNextPresentation() or getPreviousPresentation()
 * have nothing to return. Always check with isThereNextPresentation() and isTherePreviousPresentation() to avoid
 * these exceptions.
 * 
 * @author Eddy
 *
 */
public class NoSuchPresentationException extends RuntimeException {

	private static final long serialVersionUID = -1226618440106629730L;

}
