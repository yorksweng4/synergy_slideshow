/*
* Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
*/
package com.synergy.slideshow.slide;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
* Allows slideshows to present album content: groups of pictures to be flicked through at the user's leisure
* 
*@author Rory Tulip, Kat Young
*/
public class Album extends Pane {
	
	private int currentImage;	
	private Pane imageContainer;				
	private ImageView pb, nb;
	
	/**
	 * Album constructor, should use AlbumBuilder to construct properly.
	 */
	public Album(){
		super();
		
		// Initialise anything that the class will always need
		currentImage = 0;
		imageContainer = new Pane();
	}
	
	/**
	 * Builds a UI consisting of a pane in which the image is shown and 2 buttons.
	 */
	public void buildInterface(){
				
		// Create buttons
		// previous
		pb = new ImageView("left.png");
		pb.setMouseTransparent(true);
		
		//set size and position to middle of left side
		pb.setFitHeight(getPrefHeight()/3);
		pb.setFitWidth(getPrefWidth()/4);
		
		pb.setLayoutX(0);
		pb.setLayoutY(getPrefHeight()/3);
		
		// next
		nb = new ImageView("right.png");
		nb.setMouseTransparent(true);
		
		//set size and position to middle of right side
		nb.setFitHeight(getPrefHeight()/3);
		nb.setFitWidth(getPrefWidth()/4);
		
		nb.setLayoutX(getPrefWidth()/4*3);
		nb.setLayoutY(getPrefHeight()/3);
		
		// Another listener to dim graphic buttons where necessary
		// Both this listener and the one in ImageButton are needed because when
		// mouse is over button it is not seen by imageContainer
		imageContainer.setOnMouseMoved(new EventHandler<MouseEvent>() {
		    public void handle(MouseEvent me) {
		    	// if in left half, set previous button more visible
		        if (me.getX() < getPrefWidth()/2){ 
		        	pb.setOpacity(1.0);
		        	nb.setOpacity(0.3);
		        } // if in right half, set next button more visible
		        else {
		        	pb.setOpacity(0.3);
		        	nb.setOpacity(1.0);
		        }
		    }
		});
				
		// Fade buttons when mouse exits imageContainer
		imageContainer.setOnMouseExited(new EventHandler<MouseEvent>() {
		    public void handle(MouseEvent me) {
		    	pb.setOpacity(0.3);
		    	nb.setOpacity(0.3);
		    }
		});
		
		// allows imageContainer to be navigation method too
		// Both this listener and the one in ImageButton are needed because when
		// mouse is over button it is not seen by imageContainer
		imageContainer.addEventFilter(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
		    public void handle(MouseEvent me) {
		    	// if in left half, get previous Image
		        if (me.getX() < getPrefWidth()/2){
		        	previousImage();
		        } // if in right half, get next Image
		        else {
		        	nextImage();
		        }
		        
		        me.consume();
		    }
		});
		
		// consume drag events
		imageContainer.addEventFilter(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent e) {
				e.consume();
				
			}
		
			
		});

		// Gather buttons under general name: controls
		Group controls = new Group();
		controls.getChildren().addAll(pb, nb);
		
		// Add the interface to the album
		this.getChildren().addAll(imageContainer, controls);
		
	}
	
	/**
	 * This method will navigate to the previous image in the album.
	 * In truth, all of the album's images are already in the imageContainer when the App is running so
	 * what this method ACTUALLY does is set the next image visible and the previous image invisible.
	 * Additionally, this method alters the visibility of the graphic buttons based on position in the album.
	 */
	private void previousImage(){
		// A check to ensure there is an image to navigate to
		if(currentImage > 0){
			imageContainer.getChildren().get(currentImage).setVisible(false);
			currentImage = (currentImage - 1);	// Keep track of location in album
			imageContainer.getChildren().get(currentImage).setVisible(true);
			// Adjust button visibility accordingly
			if(currentImage != 0){
				pb.setVisible(true);
				nb.setVisible(true);
			}
			else{
				pb.setVisible(false);
				nb.setVisible(true);
			}
		}
	}
	
	/**
	 * This method will navigate to the next image in the album.
	 * In truth, all of the album's images are already in the imageContainer when the App is running so
	 * what this method ACTUALLY does is set the next image visible and the previous image invisible.
	 * Additionally, this method alters the visibility of the graphic buttons based on position in the album.
	 */
	private void nextImage(){
		// A check to ensure there is an image to navigate to
		if(currentImage < imageContainer.getChildren().size() - 1){
			imageContainer.getChildren().get(currentImage).setVisible(false);
			currentImage = (currentImage + 1); 	// Keep track of location in album
			imageContainer.getChildren().get(currentImage).setVisible(true);
			// Adjust button visibility accordingly
			if(currentImage != (imageContainer.getChildren().size() - 1)){
				pb.setVisible(true);
				nb.setVisible(true);
			}
			else{
				pb.setVisible(true);
				nb.setVisible(false);
			}
		}
	}
	
	/**
	 * This method take an image from it's fileLocation and builds it using the ImageBuilder.
	 * It then adds it to the imageContainer in album's UI.
	 */
	public void addImage(ImageView iv){
		// Note that the position fields are left blank as the statement below defaults the image's position to
		// the imageContainer.
		iv.setVisible(false);
		imageContainer.getChildren().add(iv);
	}	
	
	/**
	 * This method is used to kick things off and make adjustments that will be needed for the album's first viewing
	 * i.e The first image is the one initially on display and pb is dimmed.
	 */
	public void showFirstImage(){
		imageContainer.getChildren().get(0).setVisible(true);
		pb.setVisible(false);
	}
}
