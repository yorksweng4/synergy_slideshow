/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow.slide;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.media.MediaView;
import javafx.scene.media.MediaPlayer.Status;
import javafx.util.Duration;


/**
 * 
 * This extension of GridPane contains the controls for the media player; it is added to the MediaObject in the MediaBuilder if required.
 * 
 * @author Matt, Eddy
 *
 */
public class MediaControl extends GridPane {

	private Button play;
	private ProgressBar pb;
	private ProgressBar vb;
	private MediaView mv;
	private Label playTime;
	private Label volume;

	/**
	 * @param mediav the mediaview to be controlled by the controls.
	 */
	public MediaControl(MediaView mediav) {

		mv = mediav;

		setMinWidth(30);

		pb = new ProgressBar(0);

		play = new Button(">");

		// play button action
		play.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				if(mv.getMediaPlayer().getStatus() == Status.PLAYING){
					mv.getMediaPlayer().pause();
				} else if(mv.getMediaPlayer().getStatus() == Status.PAUSED || mv.getMediaPlayer().getStatus() == Status.STOPPED || mv.getMediaPlayer().getStatus() == Status.READY){
					mv.getMediaPlayer().play();
				}
			}
		});

		// if media player status changes, change what the button text says
		mv.getMediaPlayer().statusProperty().addListener(new ChangeListener<Status>() {

			@Override
			public void changed(ObservableValue<? extends Status> arg0,
					Status os, Status ns) {

				play.setText((ns == Status.PLAYING) ? "||" : ">");

			}

		});

		// stop playing when the end of the file is reached
		mv.getMediaPlayer().setOnEndOfMedia(new Runnable(){

			@Override
			public void run() {
				mv.getMediaPlayer().stop();
			}

		});

		playTime = new Label("00:00/00:00");

		pb.setMaxWidth(Double.MAX_VALUE);
		pb.setStyle("-fx-accent: red");

		// update the time display as the actual current time changes
		mv.getMediaPlayer().currentTimeProperty().addListener(new ChangeListener<Duration>() {

			@Override
			public void changed(ObservableValue<? extends Duration> d,
					Duration od, Duration nd) {
				playTime.setText(formatTime(mv.getMediaPlayer().getCurrentTime(), mv.getMediaPlayer().getMedia().getDuration()));
				pb.setProgress(nd.toMillis() / mv.getMediaPlayer().totalDurationProperty().getValue().toMillis());
			}

		});
		
		// make progress bar work with clicking
		pb.setOnMousePressed(

				new EventHandler<MouseEvent>() {

					@Override
					public void handle(MouseEvent e) {
						if(mv.getMediaPlayer().getStatus() != Status.PLAYING){
							mv.getMediaPlayer().play();
						}
						mv.getMediaPlayer().seek(Duration.seconds((e.getX() / pb.getWidth()) * mv.getMediaPlayer().totalDurationProperty().getValue().toSeconds()));
					}

				});
		// make it track when user drags
		pb.setOnMouseDragged(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent e) {
				if(e.getX() >= 0 && e.getX() <= pb.getWidth()){
					mv.getMediaPlayer().seek(Duration.seconds((e.getX() / pb.getWidth()) * mv.getMediaPlayer().totalDurationProperty().getValue().toSeconds()));
				}
			}
		});

		vb = new ProgressBar(0);

		// bind the value of vb to the actual volume
		vb.progressProperty().bind(mv.getMediaPlayer().volumeProperty());

		// make volume bar work with dragging
		vb.setOnMouseDragged(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent e) {
				if(e.getX() >= 0 && e.getX() <= vb.getWidth()){
					mv.getMediaPlayer().setVolume(e.getX()/vb.getWidth());
				}
			}
		});

		// make it work with clicking
		vb.setOnMousePressed(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent e) {
				if(e.getX() >= 0 && e.getX() <= vb.getWidth()){
					mv.getMediaPlayer().setVolume(e.getX()/vb.getWidth());
				}
			}
		});

		volume = new Label("Vol:");

		// allow pb to grow to the size of the mediacontrol
		setHgrow(pb, Priority.ALWAYS);
		vb.setPrefWidth(50);

		setHgap(5);

		// sort out columns
		ColumnConstraints playColumn = new ColumnConstraints(30);
		ColumnConstraints pbColumn = new ColumnConstraints(20, 100, Double.MAX_VALUE);
		ColumnConstraints playTimeColumn = new ColumnConstraints(75);
		ColumnConstraints volColumn = new ColumnConstraints(25);
		ColumnConstraints vbColumn = new ColumnConstraints(50);

		pbColumn.setHgrow(Priority.ALWAYS);

		getColumnConstraints().addAll(playColumn, pbColumn, playTimeColumn, volColumn, vbColumn);

		add(play, 0, 0);
		add(pb, 1, 0);
		add(playTime, 2, 0);
		add(volume, 3, 0);
		add(vb, 4, 0);

	}

	/**
	 * Display Time elapsed and length of media.
	 * @param elapsed
	 * @param duration
	 * @return the formatted time.
	 */
	private String formatTime(Duration elapsed, Duration duration) {
		int intElapsed = (int)Math.floor(elapsed.toSeconds());
		int elapsedHours = intElapsed / (60 * 60);
		if (elapsedHours > 0) {
			intElapsed -= elapsedHours * 60 * 60;
		}
		int elapsedMinutes = intElapsed / 60;
		int elapsedSeconds = intElapsed - elapsedHours * 60 * 60 
				- elapsedMinutes * 60;

		if (duration.greaterThan(Duration.ZERO)) {
			int intDuration = (int)Math.floor(duration.toSeconds());
			int durationHours = intDuration / (60 * 60);
			if (durationHours > 0) {
				intDuration -= durationHours * 60 * 60;
			}
			int durationMinutes = intDuration / 60;
			int durationSeconds = intDuration - durationHours * 60 * 60 - 
					durationMinutes * 60;
			if (durationHours > 0) {
				return String.format("%d:%02d:%02d/%d:%02d:%02d", 
						elapsedHours, elapsedMinutes, elapsedSeconds,
						durationHours, durationMinutes, durationSeconds);
			} else {
				return String.format("%02d:%02d/%02d:%02d",
						elapsedMinutes, elapsedSeconds,durationMinutes, 
						durationSeconds);
			}
		} else {
			if (elapsedHours > 0) {
				return String.format("%d:%02d:%02d", elapsedHours, 
						elapsedMinutes, elapsedSeconds);
			} else {
				return String.format("%02d:%02d",elapsedMinutes, 
						elapsedSeconds);
			}
		}
	}
}

