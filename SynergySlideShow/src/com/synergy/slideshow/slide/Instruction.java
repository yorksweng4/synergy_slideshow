/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow.slide;

import javafx.scene.Node;

/**
 * This is an instruction. Each content object contains an instance of this,
 * which lets the handler know when to make the object visible or invisible.
 * 
 * @author Eddy
 * 
 */
public class Instruction {

	private Node target;
	private int startTime;
	private int endTime;

	/**
	 * Construct an Instruction with all necessary information.
	 * 
	 * @param target a reference to the target Node.
	 * @param time the start time of the target Node in seconds.
	 * @param duration the duration of the target Node in seconds.
	 */
	public Instruction(Node target, int time, int duration) {
		this.target = target;
		this.startTime = time;
		// Convert from duration to end time for instruction sets.
		this.endTime = startTime + duration;
	}

	/**
	 * @return the target Node.
	 */
	public Node getTarget() {
		return target;
	}

	/**
	 * @param source the target Node.
	 */
	public void setTarget(Node source) {
		this.target = source;
	}

	/**
	 * @return the start time of the target in seconds.
	 */
	public int getStartTime() {
		return startTime;
	}

	/**
	 * @param time the start time of the target in seconds.
	 */
	public void setStartTime(int time) {
		this.startTime = time;
	}

	/**
	 * @return the the end time of the target in seconds.
	 */
	public int getEndTime() {
		return endTime;
	}

	/**
	 * @param duration the duration of the target in seconds.
	 */
	public void setEndTime(int duration) {
		this.endTime = duration + startTime;
	}
}
