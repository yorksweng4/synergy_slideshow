/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow.slide;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.VBox;
import javafx.scene.media.MediaView;

/**
 * This class is the Java equivalent of xml video or audio tags.
 * It can have inline controls or not, and adds a ChangeListener to visibleProperty for ease of handling.
 * 
 * @author Eddy
 *
 */
public class MediaObject extends VBox {
	
	private MediaView mv;
	private MediaControl mc;
	
	/**
	 * Construct with a mediaview.
	 * 
	 * @param mediaView the mediaview to be displayed.
	 */
	public MediaObject(MediaView mediaView){
		
		// make invisible by default
		setVisible(false);
		
		// add media view
		this.mv = mediaView;
		getChildren().add(mv);
				
		// set listener on visibility property
		visibleProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> ref, Boolean ov, Boolean nv) {
				// this allows the handler to simply setVisible, and the MediaObject takes care of itself - but it's a little tricky
				if(nv && mc != null){
					playMedia();
				} else {
					stopMedia();
				}
			}
		});
		
	}

	/**
	 * Adds the controls to the media object.
	 */
	public void addControls() {
		// build MediaControl and add to VBox
		mc = new MediaControl(mv);
		getChildren().add(mc);
	}
	
	/**
	 * Call this to stop media playback.
	 */
	public void stopMedia(){
		mv.getMediaPlayer().stop();
	}
	
	/**
	 * Call this to start media playback.
	 */
	public void playMedia(){
		mv.getMediaPlayer().play();
	}


	/**
	 * @return a reference to the mediaview in this object.
	 */
	public MediaView getMediaView() {
		return mv;	
	}
	
}
