/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow.slide;

import java.util.ArrayList;

import com.sun.javafx.scene.traversal.Direction;

import javafx.animation.Animation;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.util.Duration;


/**
 * This is an enhanced pane which holds the content nodes, but also is capable
 * of animating itself, and contains additional information such as duration and
 * instructions.
 * 
 * @author Matt, Eddy
 */
public class Slide extends Pane {

	private ArrayList<Instruction> instructions;
	private int duration;
	private TranslateTransition tt;
	private boolean moving = false;
	private boolean dragging = false;

	/**
	 * Construct a slide with all necessary information.
	 * 
	 * @param ID ID of the slide.
	 * @param dur duration of the slide in seconds.
	 * @param width the width of the slide.
	 * @param height the height of the slide.
	 */
	public Slide(String ID, int dur, double width, double height) {
		// our humble offering to the gods of extension
		super();

		// initialise things
		duration = dur;
		instructions = new ArrayList<Instruction>();
		setId(ID);
		tt = new TranslateTransition(Duration.millis(200), this);
		setPrefSize(width, height);

		// invisible by default
		setVisible(false);

	}

	/**
	 * @return the slide's instructions.
	 */
	public ArrayList<Instruction> getInstructions() {
		return instructions;
	}

	/**
	 * A quick method for adding content and its instruction.
	 * 
	 * @param content the Node to be added.
	 * @param instruction the instructions for that Node.
	 */
	public void addContent(Node content, Instruction instruction) {
		instructions.add(instruction);
		getChildren().add(content);
	}

	/**
	 * @param instruction the instruction for a given Node.
	 */
	public void addInstruction(Instruction instruction) {

		instructions.add(instruction);
	}

	/**
	 * @return true if slide is moving, false otherwise.
	 */
	public boolean isMoving() {
		return moving;
	}

	/**
	 * @return slide's duration in seconds.
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * @param duration the duration of the slide in seconds.
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * @param direction direction to enter from, use Direction.LEFT, Direction.RIGHT, Direction.UP, Direction.DOWN.
	 *            Null causes it to enter from its current position.
	 */
	public void enterFrom(Direction direction) {

		// set starting point only if animation isn't running and slide isnt being dragged
		if ((tt.getStatus() != Animation.Status.RUNNING) && (!dragging)) {
			// sort out path according to direction
			switch (direction) {
			case RIGHT:
				tt.setFromX(getPrefWidth());
				tt.setFromY(0);
				break;
			case LEFT:
				tt.setFromX(-getPrefWidth());
				tt.setFromY(0);
				break;
			case UP:
				tt.setFromX(0);
				tt.setFromY(-getPrefHeight());
				break;
			case DOWN:
				tt.setFromX(0);
				tt.setFromY(getPrefHeight());
				break;
			default:
				// do nothing, it'll enter from where it is
				break;
			}

		} else {
			// stop tt in case it is still running
			tt.stop();

			// reset everything
			tt = new TranslateTransition(Duration.millis(200), this);
		}

		// always finish centred on screen
		tt.setToX(0);
		tt.setToY(0);

		// slide must be visible
		setVisible(true);

		// what to do when the animation is finished
		tt.setOnFinished(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				moving = false;
				dragging = false;
			}
		});

		// slide is moving
		moving = true;

		// kick it off
		tt.play();

	}

	/**
	 * @param direction direction to exit to, use Direction.LEFT, Direction.RIGHT, Direction.UP, Direction.DOWN.
	 */
	public void exitTo(Direction direction) {
		// stop pt in case it is still running
		tt.stop();

		// new transition
		tt = new TranslateTransition(Duration.millis(200), this);

		switch (direction) {

		// create path based on direction
		case RIGHT:
			tt.setToX(getPrefWidth());
			break;
		case LEFT:
			tt.setToX(-getPrefWidth());
			break;
		case UP:
			tt.setToY(-getPrefHeight());
			break;
		case DOWN:
			tt.setToY(getPrefHeight());
			break;
		default:
			tt.setToX(getPrefWidth());
			break;

		}

		// what to do when the animation is finished
		tt.setOnFinished(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				setVisible(false);
				resetContent();
				moving = false;
				dragging = false;
			}
		});

		// slide is moving
		moving = true;

		// kick things off
		tt.play();
	}

	/**
	 * This method stops all the media on the slide from playing.
	 */
	public void stopMedia() {
		for (int i = 0; i < getChildren().size(); i++) {
			if (getChildren().get(i).getClass().toString().contains("MediaObject")) {
				// stop
				((MediaObject) getChildren().get(i)).getMediaView().getMediaPlayer().stop();
			}
		}
	}

	/**
	 * Call this to reset the contents of the slide to time 0 status.
	 */
	public void resetContent() {
		// this loop cycles through all the instructions for this slide
		for (int i = 0; i < instructions.size(); i++) {
			// visible only if start time is 0
			instructions.get(i).getTarget().setVisible(instructions.get(i).getStartTime() == 0);
		}
	}

	/**
	 * @param b true if dragging, false otherwise.
	 */
	public void setDragging(boolean b) {
		dragging = b;
	}

	/**
	 * @return true if being dragged, false otherwise.
	 */
	public boolean isDragging() {
		return dragging;
	}
}
