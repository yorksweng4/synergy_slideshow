/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.slideshow;

import java.util.ArrayList;

import com.synergy.slideshow.slide.Instruction;

/**
 * enum class for lifecycle management, holds the three possible states of
 * the slideshow.
 * 
 * RESUMED - the slideshow is visible and active.
 * PAUSED - the slideshow is invisible, content is not handled.
 * DEAD - the slideshow has been killed and can't be used again unless a new instance is made.
 * 
 * @author Eddy
 * 
 */
enum SlideShowState {
	RESUMED, PAUSED, DEAD;
};

/**
 * Content handler with integrated time thread. Handles objects as well as the
 * time for the SlideShow, and is controlled by changing its state.
 *  
 * @author Eddy
 * 
 */
public class ContentHandler extends Thread {

	private SlideShowState slideShowState;
	private boolean resetTime = true;
	private Object monitor;

	private SlideShow parent;

	/**
	 * @param slideShow a reference to the parent slideshow
	 */
	public ContentHandler(SlideShow slideShow) {
		slideShowState = SlideShowState.PAUSED;
		this.parent = slideShow;
		monitor = new Object();
		
		// daemon threads die automatically when no non-daemon threads are left
		setDaemon(true);

		start();
	}

	/**
	 * Defines the behaviour of the thread. This method shouldn't really be
	 * explicitly called anywhere.
	 * 
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {

		// time starts at 0
		int time = 0;

		// as long as the slideshow is not dead, execute this loop
		while (slideShowState != SlideShowState.DEAD) {

			// is the slideshow resumed?
			if (slideShowState == SlideShowState.RESUMED) {

				// sleep for one second and increment time, unless resetting
				if (resetTime) {

					time = 0;
					resetTime = false;
				} else {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// user probably pressed next, stop sleeping and move on
					}
					time++;
				}

				// refresh slide content
				updateContent(parent.getCurrentSlide().getInstructions(), time);

				// if slide duration has been reached, move on - but only if
				// duration is greater than 0 and time is not resetting
				if ((parent.getCurrentSlide().getDuration() <= time) && (parent.getCurrentSlide().getDuration() > 0) && (!resetTime)) {
					// if we have a next slide, go to it - if not, end the slideshow
					if(parent.isThereNextSlide()){
						parent.goToNextSlide();
					} else if(parent.isThereNextPresentation()) {
						parent.goToNextPresentation();
					} else {
						parent.pause();
					}
					
				}
			}

			// or is it paused?
			else if (slideShowState == SlideShowState.PAUSED) {
				// wait on monitor
				synchronized (monitor) {
					try {
						monitor.wait();
					} catch (InterruptedException e) {
						// either the app was closed or the user resumed the
						// slideshow, either way move on
					}
				}
				resetTime = true;
			}
		}
	}

	/**
	 * Call this to reset the time, usually when the slide changes.
	 */
	public void resetTime() {
		resetTime = true;
		interrupt();
	}

	/**
	 * @param s the state to set, use SlideShowState.RESUMED,
	 *            SlideShowState.PAUSED and SlideShowState.DEAD
	 */
	public void setSlideShowState(SlideShowState s) {
		this.slideShowState = s;
	}

	/**
	 * @return the current slideshow state
	 */
	public SlideShowState getSlideShowState() {
		return slideShowState;
	}

	/**
	 * notifies the thread monitor
	 */
	public void notifyThread() {
		synchronized (monitor) {
			monitor.notify();
		}
	}

	/**
	 * 
	 * This method handles content, including resetting, which is automatically
	 * done when time is 0.
	 * 
	 * @param instructions this is the list of instructions for objects relative to the
	 *            current slide
	 * @param time the current time
	 */

	public void updateContent(ArrayList<Instruction> instructions, int time) {

		// this loop cycles through all the instructions for this slide
		for (int i = 0; i < instructions.size(); i++) {

			// check if something is supposed to change
			if ((instructions.get(i).getEndTime() == time) || (instructions.get(i).getStartTime() == time) || (time <= 0)) {
				// make the change
				instructions.get(i).getTarget().setVisible(instructions.get(i).getStartTime() == time);
			}
		}
	}
}
