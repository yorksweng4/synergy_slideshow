package com.synergy.testenvironment;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.geometry.Orientation;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.synergy.slideshow.SlideShow;
import com.synergy.slideshow.parser.SynergyXMLReader;

public class Test extends Application {

	private Pane testPane;
	private SynergyXMLReader sxr;
	private SlideShow ss;
	private Text text;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch();

	}

	@Override
	public void start(Stage stage) throws Exception {

		testPane = new Pane();
		testPane.setPrefSize(800, 600);
		stage.setScene(new Scene(testPane));
		
		text = new Text();
		text.setFont(Font.font("Arial", 18));
		text.setTextOrigin(VPos.TOP);
		
		testPane.getChildren().add(text);
		
		stage.show();

		Task<Void> task = new Task<Void>() {
			@Override
			public Void call() {

				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						//Loading.showBusy();
					}
				});

				try{
					sxr = new SynergyXMLReader();
					sxr.progressProperty().addListener(new ChangeListener<Number>() {

						@Override
						public void changed(
								ObservableValue<? extends Number> arg0,
								Number ov, Number nv) {
							
							text.setText("File: "+sxr.fileNameProperty().get()+"\nProgress: "+sxr.progressProperty().get());
							
						}
					});
					sxr.parseFile("http://benshouse.crabdance.com/xml/POI/Exhibition%20Centre.xml");
					//sxr.parseFile("M:/excentre/1WelcomePage.xml");
					//sxr.parseFile("M:/SWEng/TestXML.xml");
				} catch(SAXParseException spe){
					// error handling goes here
					System.out.println("Column: "+spe.getColumnNumber());
					System.out.println("Line: "+spe.getLineNumber());
					System.out.println("Message: "+spe.getMessage());
				} catch(SAXException se) {
					// and here
					System.out.println("Unreachable XML: "+se.getMessage());
				}

				return null;
			}

			@Override
			protected void succeeded() {
				super.succeeded();
				if(sxr.getPresentable() != null){

					ss = new SlideShow(sxr.getPresentable());

					ss.setOrientation(Orientation.HORIZONTAL);

					// display and play :)
					testPane.getChildren().add(ss);
					ss.play();
				}

				//Loading.hideBusy();
			}

		};
		new Thread(task).start();

	}

}
